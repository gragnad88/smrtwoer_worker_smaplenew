package com.samwoo.sampleworker.Service;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;

import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtilsLocalService;
import com.samwoo.sampleworker.MainActivity;
import com.samwoo.sampleworker.R;
import com.samwoo.sampleworker.SmartWorkApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BackGroundService extends Service {
    SmartWorkApplication mApp;
    //
    private static final int CAMERA_REQUEST_CODE = 2320;
    private static final int VIDEO_REQUEST_CODE = 2321;
    private static final int LOGOUT_FINISH = 7000;

    TextView workerName, workerNumber;

    Handler mHandler = null;
    boolean isThreadStart = false;
    String gMData = "[No Barcode]";

    String type = null;

    String fileName;
    private final String rootSD = Environment.getExternalStorageDirectory().toString();

    String save_Path;
    String save_folder;
    DownloadThread dThread;

    AlertDialog dialog;
    AlertDialog loadDialog;
    ProgressDialog mentoringDialog;

    DBUtilsLocalService request = null /*new DBUtils()*/;
    DBUtilsLocalService auth = null /*new DBUtils()*/;
    DBUtilsLocalService checklist = null;

    ArrayList<HashMap<String, String>> request_list = new ArrayList<>();
    HashMap<String, String> request_id = new HashMap<>();
    String request_index;

    boolean isCall;

    long mentoringRequestTime;

    boolean isDownload = false;
    boolean mRequestCompleted = false;
    boolean mAuthCompleted = false;
    long startMills = 0;
    long targetMills = 0;
    long sleepMills = 0;
    //
    String downloadType;
    //
    BlockingQueue<DBUtilsLocalService.QueryResultData> mServiceBackBlockQueue;
    Thread DBQueryWorkerThread;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mApp = (SmartWorkApplication)getApplication();

        mServiceBackBlockQueue = new ArrayBlockingQueue<>(20);

        Log.i("BackGround Service : ","Start");
        request_index = mApp.getAuthID();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what < 3) {
                    NotificationCompat.Builder  mBuilder= new NotificationCompat.Builder(BackGroundService.this);
                    String title = null;
                    Intent i = new Intent(BackGroundService.this, MainActivity.class);
                    // FLAG_IMMUTABLE / FLAG_UPDATE_CURRENT
                    PendingIntent pendingIntent = PendingIntent.getActivity(BackGroundService.this, 0, i, PendingIntent.FLAG_IMMUTABLE);
                    switch (msg.what) {
                        case 0:
                            type = "image";
                            mBuilder.setContentText("매니저가 " + type + " 보냈습니다.");
                            break;
                        case 1:
                            type = "video";
                            mBuilder.setContentText("매니저가 " + type + " 보냈습니다.");
                            break;
                        case 2:
                            type = "text";
                            mBuilder.setContentText("매니저가 " + type + " 보냈습니다.");
                    }

                    downloadType = type;

                    mBuilder.setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("VARLOS")
                            .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)
                            .setTicker("Ticker")
                            .setWhen(System.currentTimeMillis());

                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.notify(type,msg.what, mBuilder.build());
                }
            }
        };

        request = new DBUtilsLocalService(DBUtilsLocalService.DBTableType.REQUEST_TABLE,mServiceBackBlockQueue);
        auth = new DBUtilsLocalService(DBUtilsLocalService.DBTableType.AUTH_TABLE,mServiceBackBlockQueue);
        //
        request.setDB(Constant.TRANSMISSION,new String[]{"u_index", "u_type", "u_url", "u_state"});
        auth.setDB(Constant.AUTH,new String[]{"userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" });
        //
        DBQueryWorkerThread = new Thread(DBQueryManager);
        DBQueryWorkerThread.start();

        return super.onStartCommand(intent, flags, startId);
    }



    @Override
    public ComponentName startService(Intent service) {

        return super.startService(service);
    }


    ArrayList<HashMap<String,String>> mRequestList = new ArrayList<>();
    ArrayList<HashMap<String, String>> mAuthList = new ArrayList<>();

    public void processDBQueryResult( ArrayList<HashMap<String,String>> mRequestList,ArrayList<HashMap<String, String>> mAuthList) {
        request_list.clear();
        request_id.clear();

        JSONArray objList = new JSONArray();
        if (objList.length() != 0 ) for (int i = 0; i < objList.length(); i++) objList.remove(i);

        for (int i = 0; i < mRequestList.size(); i++) {
            if (Objects.requireNonNull(mRequestList.get(i).get("u_index")).equals(request_index)) {
                HashMap<String, String> list = new HashMap<>();

                list.put("id", mRequestList.get(i).get("id"));
                list.put("u_index", mRequestList.get(i).get("u_index"));
                list.put("u_type", mRequestList.get(i).get("u_type"));
                list.put("u_url", mRequestList.get(i).get("u_url"));
                list.put("u_state", mRequestList.get(i).get("u_state"));

                request_id.put(mRequestList.get(i).get("u_type"), mRequestList.get(i).get("id"));

                request_list.add(list);
            }
        }
        int updateLevel = 10;
        Iterator<HashMap<String, String>> itr = request_list.iterator();
        while (itr.hasNext()) {
            HashMap<String, String> obj = itr.next();
            Message msg;
            if (obj.get("u_state").equals("Transmission")) {
                mApp.setUrl(obj.get("u_url"));

                switch (obj.get("u_type")) {
                    case "image":
                        objList.put(DB_reset(obj, true));
                        msg = Message.obtain(null,0);
                        msg.obj = objList;
                        mHandler.sendMessage(msg);
                        updateLevel=  0;
                        break;
                    case "video":
                        objList.put(DB_reset(obj, true));
                        msg = Message.obtain(null,1);
                        msg.obj = objList;
                        mHandler.sendMessage(msg);
                        updateLevel= 1;
                        break;
                    case "text":
                        objList.put(DB_reset(obj, true));
                        msg = Message.obtain(null,2);
                        msg.obj = objList;
                        mHandler.sendMessage(msg);
                        updateLevel =2;
                        break;
                }
            }
        }

        if (objList.length() != 0) {
            if (updateLevel > 3) {
                //request.update(true, objList);
            }
        }
        else {
            LaunchWorkerThread();
        }
    }

    private final Runnable DBQueryManager = new Runnable()
    {
        @Override
        public void run() {
            Log.i("launch","DBQueryManager Start");
            //
            boolean isExit = false;
            //
            LaunchWorkerThread();
            //
            while (!isExit) {
                try {
                    Log.i("Thread", "Run Check");
                    {
                        DBUtilsLocalService.QueryResultData data = mServiceBackBlockQueue.take();
                        Log.i("Thread", "Data in");
                        switch (data.mTranSactionType) {

                            case DBUtilsLocalService.TransactionType.READ:
                                if (data.DBTableType == DBUtilsLocalService.DBTableType.REQUEST_TABLE) {
                                    mRequestList = data.listData;
                                    mRequestCompleted = true;
                                } else if (data.DBTableType == DBUtilsLocalService.DBTableType.AUTH_TABLE) {
                                    mAuthList = data.listData;
                                    mAuthCompleted = true;
                                } else {
                                    //
                                }
                                if (mRequestCompleted && mAuthCompleted) {
                                    mRequestCompleted = false;
                                    mAuthCompleted = false;
                                    Log.i("launch", "BackGround");
                                    processDBQueryResult(mRequestList,mAuthList);
                                }
                                break;

                            case DBUtilsLocalService.TransactionType.UPDATE:
                            case DBUtilsLocalService.TransactionType.DELETE:
                            case DBUtilsLocalService.TransactionType.INSERT:
                                //Update, Insert, Delete  End Retire Read
                                LaunchWorkerThread();
                                //
                                break;

                            case DBUtilsLocalService.TransactionType.TRANSACTION_EXIT:
                                isExit = true;
                                break;
                        }
                    }
                } catch (InterruptedException e) {
                    isExit = true;
                    mServiceBackBlockQueue.clear();
                }
            }
        }
    };


    void intDataSwitch() {
        if (mApp.getUrl().endsWith("mp4") || mApp.getUrl().endsWith("avi")) {
            save_folder = "/Movies";
            fileDownload();
        } else if (mApp.getUrl().endsWith("jpg") || mApp.getUrl().endsWith("jpeg")
                || mApp.getUrl().endsWith("JPG") || mApp.getUrl().endsWith("gif")
                || mApp.getUrl().endsWith("png") || mApp.getUrl().endsWith("bmp")) {
            save_folder = "/Pictures";
            fileDownload();
        } else if (mApp.getUrl().endsWith("txt") || mApp.getUrl().endsWith("doc")
                || mApp.getUrl().endsWith("docx") || mApp.getUrl().endsWith("xls")
                || mApp.getUrl().endsWith("xlsx") || mApp.getUrl().endsWith("ppt")
                || mApp.getUrl().endsWith("pptx") || mApp.getUrl().endsWith("pdf")) {
            save_folder = "/Documents";
            fileDownload();
        } else {
//            checklist = new DBUtilsLocalService();
//            checklist.setDB(this, Constant.CHECKLIST, new String[]{ "name", "date", "worker", "capture" });
//            checklist.read(DBUtilsLocalService.DBTableType.CHECKLIST_READ, mHandler);
        }
    }

    public void fileDownload() {
        File dir = new File(save_Path + save_folder);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (!new File(save_Path + save_folder + "/" + mApp.getUrl()).exists()) {
            dThread = new DownloadThread(Constant.FILE_PATH + "/" + mApp.getUrl(),
                    save_Path + save_folder + "/" + mApp.getUrl());
            dThread.start();
        } else mHandler.sendEmptyMessage(6);
    }


    private JSONObject DB_reset(HashMap<String, String> hashObj, boolean type) {
        String id = hashObj.get("id");

        JSONObject obj = new JSONObject();

        try {
            obj.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (type) {
            if (Objects.requireNonNull(hashObj.get("u_type")).equals("image")) this.type = "이미지";
            else if (Objects.requireNonNull(hashObj.get("u_type")).equals("video")) this.type = "동영상";
            else if (Objects.requireNonNull(hashObj.get("u_type")).equals("text")) this.type = "문서";
        }
        return obj;
    }

    Thread LaunchWorkerCheck;

    public void LaunchWorkerThread()
    {
        LaunchWorkerCheck = new Thread(BackgroundRunnable);
        LaunchWorkerCheck.start();
    }
    //LocalService add Data==========================================================End
    private final Runnable BackgroundRunnable = new Runnable() {
        @Override
        public void run() {
            sleepMills = 0;

            if (startMills == 0) {
                startMills = System.currentTimeMillis();
                targetMills = startMills + 1000;

                sleepMills = 1000;

            } else {
                long curMills = System.currentTimeMillis();
                sleepMills = targetMills - curMills;

                if (sleepMills > 0) {
                    targetMills += 1000;
                } else {
                    startMills = System.currentTimeMillis();
                    targetMills = startMills + 1000;
                    sleepMills = 0;
                }
            }

            try {
                Log.i("sleepMills",  String.valueOf(sleepMills));
                Log.i("+sleepMills",  String.valueOf(System.currentTimeMillis()));
                Thread.sleep(sleepMills);
                //
                request.read(true);
                auth.read(true);

                //
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    } ;


    private class DownloadThread extends Thread {
        String ServerUrl;
        String LocalPath;

        DownloadThread(String serverPath, String localPath) {
            ServerUrl = serverPath;
            LocalPath = localPath;
        }

        @Override
        public void run() {
            URL imgurl;
            int Read;
            try {
                imgurl = new URL(ServerUrl);
                HttpURLConnection conn = (HttpURLConnection) imgurl.openConnection();
                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];
                InputStream is = conn.getInputStream();
                File file = new File(LocalPath);
                FileOutputStream fos = new FileOutputStream(file);

                for (;;) {
                    Read = is.read(tmpByte);
                    if (Read <= 0) break;
                    fos.write(tmpByte, 0, Read);
                }

                is.close();
                fos.close();
                conn.disconnect();
            } catch (MalformedURLException e) {
                Log.e("ERROR1", e.getMessage());
            } catch (IOException e) {
                Log.e("ERROR2", e.getMessage());
                e.printStackTrace();
            }
            mHandler.sendEmptyMessage(6);
        }
    }


    @Override
    public void onDestroy() {
        Log.i("BackGround Service : ","Stop");

        LaunchWorkerCheck.interrupt();
        DBQueryWorkerThread.interrupt();

        super.onDestroy();
    }


    @Override
    public boolean stopService(Intent name) {

        Log.i("BackGround Service : ","Stop");

        return super.stopService(name);
    }
}


