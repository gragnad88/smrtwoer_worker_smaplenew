package com.samwoo.sampleworker.Service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.samwoo.sampleworker.CheckListInfoActivity;
import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;
import com.samwoo.sampleworker.ScanLoginActivity;
import com.samwoo.sampleworker.MentoringActivity;
import com.samwoo.sampleworker.SmartWorkApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class OtherAlarmService extends Service {
    SmartWorkApplication mApp;

    //
    private static final int CAMERA_REQUEST_CODE = 2320;
    private static final int VIDEO_REQUEST_CODE = 2321;
    private static final int LOGOUT_FINISH = 7000;

    TextView workerName, workerNumber;

    Handler mHandler = null;
    boolean isThreadStart = false;
    String gMData = "[No Barcode]";

    String type = null;

    String fileName;
    private final String rootSD = Environment.getExternalStorageDirectory().toString();

    String save_Path;
    String save_folder;
    DownloadThread dThread;

    AlertDialog dialog;
    AlertDialog loadDialog;
    ProgressDialog mentoringDialog;

    DBUtils qr_code = null /*new DBUtils()*/;
    DBUtils request = null /*new DBUtils()*/;
    DBUtils auth = null /*new DBUtils()*/;
    DBUtils checklist = null;

    ArrayList<HashMap<String, String>> request_list = new ArrayList<>();
    HashMap<String, String> request_id = new HashMap<>();
    String request_index;

    boolean isCall;

    long mentoringRequestTime;

    boolean isDownload = false;
    boolean mRequestCompleted = false;
    boolean mAuthCompleted = false;
    long startMills = 0;
    long targetMills = 0;
    long sleepMills = 0;
    //
    String downloadType;
    //
    BlockingQueue<DBUtils.QueryResultData> mServiceBackBlockQueue;
    Thread DBQueryWorkerThread;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("HandlerLeak")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mApp = (SmartWorkApplication)getApplication();

        mServiceBackBlockQueue = new ArrayBlockingQueue<>(10);

        Log.i("BackGround Service : ","Start");

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what < 3) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OtherAlarmService.this);
                    String title = null;

                    switch (msg.what) {
                        case 0:
                            type = "image";
                            title = "이미지";
                            save_folder = "/Pictures";
                            break;
                        case 1:
                            type = "video";
                            title = "동영상";
                            save_folder = "/Movies";
                            break;
                        case 2:
                            type = "text";
                            title = "문서";
                            save_folder = "/Documents";
                            break;
                    }

                    downloadType = type;

                    builder.setTitle(title)
                            .setMessage("파일이 수신되었습니다")
                            .setPositiveButton("보기", (dialog, which) -> {
                                if (!isDownload) {
                                    isDownload = true;

                                    mApp.setControlVideo(false);
                                    request.update(request_id.get(downloadType), new String[]{ mApp.getAuthID(), downloadType, "", "Accept" });

                                    fileDownload();
                                }
                            })
                            .setNegativeButton("닫기", (dialog, which) -> request.update(request_id.get(downloadType), new String[]{ mApp.getAuthID(), downloadType, "", "Cancel" }));

                    builder.setCancelable(false);

                    dialog = builder.create();
                    if(!dialog.isShowing()) dialog.show();
                } else if (msg.what == 3) {
                    mApp.setControlVideo(true);
                    for (int i = 0; i < qr_code.getContactList().size(); i++) {
                        if (gMData.equals(qr_code.getContactList().get(i).get("data"))) {
                            mApp.setUrl(qr_code.getContactList().get(i).get("data"));
                            intDataSwitch();
                            gMData = "[No Barcode]";
                            break;
                        }
                        if (i + 1 == qr_code.getContactList().size()) {
                            Toast.makeText(OtherAlarmService.this, "등록된 정보가 없습니다. 확인 후 다시 인식해 주세요.", Toast.LENGTH_LONG).show();
                        }
                    }
                } else if (msg.what == 6) {
                    isDownload = false;

                    // TODO Auto-generated method stub
                    // 파일 다운로드 종료 후 다운받은 파일을 실행시킨다.
                    //showDownloadFile();
                } else if (msg.what == 7) {
                    // 방 만드는 시간을 위해 대기
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    mentoringDialog.dismiss();

                    Intent i = new Intent(OtherAlarmService.this, MentoringActivity.class);
                    startActivity(i);
                } else if (msg.what == 8) {
                    isCall = true;
                    Intent i = new Intent(OtherAlarmService.this, MentoringActivity.class);
                    startActivity(i);
                } else if (msg.what == DBUtils.DBTableType.CHECKLIST_READ) {
                    for (int i = 0; i < checklist.getContactList().size(); i++) {
                        if (Objects.requireNonNull(checklist.getContactList().get(i).get("name")).equals(mApp.getUrl()))
                            mApp.setChecklist(checklist.getContactList().get(i).get("id"));
                    }
                    Intent intent = new Intent(getApplicationContext(), CheckListInfoActivity.class);
                    startActivity(intent);
                } else if (msg.what == LOGOUT_FINISH) {
                    Intent i = new Intent(getApplicationContext(), ScanLoginActivity.class);
                    startActivity(i);
                }
            }
        };

        request = new DBUtils(DBUtils.DBTableType.REQUEST_TABLE,mServiceBackBlockQueue);
        auth = new DBUtils(DBUtils.DBTableType.AUTH_TABLE,mServiceBackBlockQueue);
        //
        request.setDB(Constant.TRANSMISSION,new String[]{"u_index", "u_type", "u_url", "u_state"});
        auth.setDB(Constant.AUTH,new String[]{"userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" });
        //
        DBQueryWorkerThread = new Thread(DBQueryManager);
        DBQueryWorkerThread.start();

        return super.onStartCommand(intent, flags, startId);
    }

    public void processDBQueryResult() {
        request_list.clear();
        request_id.clear();

        JSONArray objList = new JSONArray();
        if (objList.length() != 0 ) for (int i = 0; i < objList.length(); i++) objList.remove(i);

        for (int i = 0; i < request.getContactList().size(); i++) {
            if (Objects.requireNonNull(request.getContactList().get(i).get("u_index")).equals(request_index)) {
                HashMap<String, String> list = new HashMap<>();

                list.put("id", request.getContactList().get(i).get("id"));
                list.put("u_index", request.getContactList().get(i).get("u_index"));
                list.put("u_type", request.getContactList().get(i).get("u_type"));
                list.put("u_url", request.getContactList().get(i).get("u_url"));
                list.put("u_state", request.getContactList().get(i).get("u_state"));

                request_id.put(request.getContactList().get(i).get("u_type"), request.getContactList().get(i).get("id"));

                request_list.add(list);
            }
        }

        Iterator<HashMap<String, String>> itr = request_list.iterator();
        while (itr.hasNext()) {
            HashMap<String, String> obj = itr.next();

            if (obj.get("u_state").equals("Transmission")) {
                mApp.setUrl(obj.get("u_url"));

                switch (obj.get("u_type")) {
                    case "image":
                        objList.put(DB_reset(obj, true));
                        mHandler.sendEmptyMessage(0);
                        break;
                    case "video":
                        objList.put(DB_reset(obj, true));
                        mHandler.sendEmptyMessage(1);
                        break;
                    case "text":
                        objList.put(DB_reset(obj, true));
                        mHandler.sendEmptyMessage(2);
                        break;
                }
            } else if (obj.get("u_state").equals("Response")) {
                if (obj.get("u_type").equals("videoCall")) {
                    if (mentoringDialog.isShowing()) {
                        objList.put(DB_reset(obj, false));

                        mHandler.sendEmptyMessage(7);
                    } else {
                        // recall 에 대한 코딩
                    }
                }
            }

            if (mentoringRequestTime != 0) {
                long mentoringTimeOver = System.currentTimeMillis();

                if (mentoringTimeOver - mentoringRequestTime >= 30 * 1000) {
                    if (mentoringDialog.isShowing()) {
                        request.update(request_id.get("videoCall"), new String[]{ obj.get("u_index"), "videoCall", "", "Missed" });

                        mentoringDialog.dismiss();
                        mentoringRequestTime = 0;
                    }

                }
            }
        }

        if (objList.length() != 0) {
            request.update(true,objList);
        }
        else {
            LaunchWorkerThread();
        }

    }

    private final Runnable DBQueryManager = new Runnable()
    {
        @Override
        public void run() {
            Log.i("launch","DBQueryManager Start");
            //
            boolean isExit = false;
            //
            LaunchWorkerThread();
            //
            while (!isExit) {
                try {
                    Log.i("Thread", "Run Check");
                    {
                        DBUtils.QueryResultData data = mServiceBackBlockQueue.take();
                        Log.i("Thread", "Data in");
                        switch (data.mTranSactionType) {

                            case DBUtils.TransactionType.READ:
                                if (data.DBTableType == DBUtils.DBTableType.REQUEST_TABLE) {
                                    request.contactList = data.listData;
                                    mRequestCompleted = true;
                                } else if (data.DBTableType == DBUtils.DBTableType.AUTH_TABLE) {
                                    auth.contactList = data.listData;
                                    mAuthCompleted = true;
                                } else {
                                    //
                                }
                                if (mRequestCompleted && mAuthCompleted) {
                                    mRequestCompleted = false;
                                    mAuthCompleted = false;
                                    Log.i("launch", "BackGround");
                                    processDBQueryResult();
                                }
                                break;

                            case DBUtils.TransactionType.UPDATE:
                            case DBUtils.TransactionType.DELETE:
                            case DBUtils.TransactionType.INSERT:
                                //Update, Insert, Delete  End Retire Read
                                LaunchWorkerThread();
                                //
                                break;

                            case DBUtils.TransactionType.TRANSACTION_EXIT:
                                isExit = true;
                                break;
                        }
                    }
                } catch (InterruptedException e) {
                    isExit = true;
                    //exit messageQueue data Clear Need
                    mServiceBackBlockQueue.clear();
                }
            }
        }
    };


    void intDataSwitch() {
        if (mApp.getUrl().endsWith("mp4") || mApp.getUrl().endsWith("avi")) {
            save_folder = "/Movies";
            fileDownload();
        } else if (mApp.getUrl().endsWith("jpg") || mApp.getUrl().endsWith("jpeg")
                || mApp.getUrl().endsWith("JPG") || mApp.getUrl().endsWith("gif")
                || mApp.getUrl().endsWith("png") || mApp.getUrl().endsWith("bmp")) {
            save_folder = "/Pictures";
            fileDownload();
        } else if (mApp.getUrl().endsWith("txt") || mApp.getUrl().endsWith("doc")
                || mApp.getUrl().endsWith("docx") || mApp.getUrl().endsWith("xls")
                || mApp.getUrl().endsWith("xlsx") || mApp.getUrl().endsWith("ppt")
                || mApp.getUrl().endsWith("pptx") || mApp.getUrl().endsWith("pdf")) {
            save_folder = "/Documents";
            fileDownload();
        } else {
            checklist = new DBUtils();
            checklist.setDB(this, Constant.CHECKLIST, new String[]{ "name", "date", "worker", "capture" });
            checklist.read(DBUtils.DBTableType.CHECKLIST_READ, mHandler);
        }
    }

    public void fileDownload() {
        File dir = new File(save_Path + save_folder);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (!new File(save_Path + save_folder + "/" + mApp.getUrl()).exists()) {
            dThread = new DownloadThread(Constant.FILE_PATH + "/" + mApp.getUrl(),
                    save_Path + save_folder + "/" + mApp.getUrl());
            dThread.start();
        } else mHandler.sendEmptyMessage(6);
    }


    private JSONObject DB_reset(HashMap<String, String> hashObj, boolean type) {
        String id = hashObj.get("id");

        JSONObject obj = new JSONObject();

        try {
            obj.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (type) {
            if (Objects.requireNonNull(hashObj.get("u_type")).equals("image")) this.type = "이미지";
            else if (Objects.requireNonNull(hashObj.get("u_type")).equals("video")) this.type = "동영상";
            else if (Objects.requireNonNull(hashObj.get("u_type")).equals("text")) this.type = "문서";
        }
        return obj;
    }

    Thread LaunchWorkerCheck;

    public void LaunchWorkerThread()
    {
        LaunchWorkerCheck = new Thread(BackgroundRunnable);
        LaunchWorkerCheck.start();
    }
    //LocalService add Data==========================================================End
    private final Runnable BackgroundRunnable = new Runnable() {
        @Override
        public void run() {
            sleepMills = 0;

            if (startMills == 0) {
                startMills = System.currentTimeMillis();
                targetMills = startMills + 1000;

                sleepMills = 1000;

            } else {
                long curMills = System.currentTimeMillis();
                sleepMills = targetMills - curMills;

                if (sleepMills > 0) {
                    targetMills += 1000;
                } else {
                    startMills = System.currentTimeMillis();
                    targetMills = startMills + 1000;
                    sleepMills = 0;
                }
            }

            try {
                Log.i("sleepMills",  String.valueOf(sleepMills));
                Log.i("+sleepMills",  String.valueOf(System.currentTimeMillis()));
                Thread.sleep(sleepMills);
                //
                request.read(true);
                auth.read(true);
                //
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    } ;


    private class DownloadThread extends Thread {
        String ServerUrl;
        String LocalPath;

        DownloadThread(String serverPath, String localPath) {
            ServerUrl = serverPath;
            LocalPath = localPath;
        }

        @Override
        public void run() {
            URL imgurl;
            int Read;
            try {
                imgurl = new URL(ServerUrl);
                HttpURLConnection conn = (HttpURLConnection) imgurl.openConnection();
                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];
                InputStream is = conn.getInputStream();
                File file = new File(LocalPath);
                FileOutputStream fos = new FileOutputStream(file);

                for (;;) {
                    Read = is.read(tmpByte);
                    if (Read <= 0) break;
                    fos.write(tmpByte, 0, Read);
                }

                is.close();
                fos.close();
                conn.disconnect();
            } catch (MalformedURLException e) {
                Log.e("ERROR1", e.getMessage());
            } catch (IOException e) {
                Log.e("ERROR2", e.getMessage());
                e.printStackTrace();
            }
            mHandler.sendEmptyMessage(6);
        }
    }

    @Override
    public void onDestroy() {
        Log.i("BackGround Service : ","Stop");

        LaunchWorkerCheck.interrupt();
        DBQueryWorkerThread.interrupt();

        super.onDestroy();
    }

    @Override
    public boolean stopService(Intent name) {

        Log.i("BackGround Service : ","Stop");

        return super.stopService(name);
    }
}
