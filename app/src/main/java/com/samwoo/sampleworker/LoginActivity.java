package com.samwoo.sampleworker;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.WindowManager;

import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

public class LoginActivity extends Activity {
    SmartWorkApplication mApp;
//    EditText id_edit, pw_edit;
//    Button loginBtn, loginQRBtn;

    private CaptureManager capture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_login);

        mApp = (SmartWorkApplication)getApplication();

//        id_edit = (EditText)findViewById(R.id.id_edit);
//        pw_edit = (EditText)findViewById(R.id.pw_edit);
//        loginBtn = (Button)findViewById(R.id.loginBtn);
//        loginQRBtn = (Button)findViewById(R.id.loginQRBtn);

        DecoratedBarcodeView barcodeScannerView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if(mApp.isLoading()) {
            finishAffinity();
            System.runFinalization();
            System.exit(0);
        }
    }
}
