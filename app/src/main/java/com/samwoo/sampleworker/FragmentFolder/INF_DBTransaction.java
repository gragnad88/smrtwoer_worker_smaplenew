package com.samwoo.sampleworker.FragmentFolder;

import com.samwoo.sampleworker.DB.DBUtilsNew;

import java.util.ArrayList;

public interface INF_DBTransaction<S> {
    void Request(DBUtilsNew.DBTransaction[] data);
    void Request(DBUtilsNew.DBTransaction data);
    void ResponseConTactData(S data,int TableID);
}
