package com.samwoo.sampleworker.FragmentFolder;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.samwoo.sampleworker.DB.DBUtilsNew;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class FragmentControllerClass<T> extends Fragment implements INF_DBTransaction<ArrayList<HashMap<String,String>>>{

    public int mFragmentState;
    boolean onceTimeFragment = false;
    public int mDBCount = 0;
    //
    HashMap<Integer,T> DBTableHashMap;
    //
    public Context mContext;

    protected Handler fragHandler;

    public int getmDBCount() {
        return DBTableHashMap.size();
    }

    interface FragmentStatus
    {
        int STATUS_WAIT = 5001;
        int STATUS_ACTIVE = 5002;
        int STATUS_DEACTIVATE = 5003;
    }

    public boolean isOnceTimeFragment() {
        return onceTimeFragment;
    }

    //abstract Function
    //ProcessDBQueryResult
    public abstract void ProcessDBResult();
    //getter ans setter
    public void setmDBCount(int mDBCount) {
        this.mDBCount = mDBCount;
    }

    public void setOnceTimeFragment(boolean onceTimeFragment) {
        this.onceTimeFragment = onceTimeFragment;
    }

    public Handler getFragHandler() {
        return fragHandler;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }
    //
}
