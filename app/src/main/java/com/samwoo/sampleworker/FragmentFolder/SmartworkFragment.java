package com.samwoo.sampleworker.FragmentFolder;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtilsLocalService;
import com.samwoo.sampleworker.DB.DBUtilsNew;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class SmartworkFragment extends  FragmentControllerClass<DBUtilsNew> {

    //TestFragment DB Read Data===============================
    public ArrayList<HashMap<String,String>> firstContactData,secondContactData;
    int responseCount = 0;
    //==============================================Delete Need
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TestFragment DB Read Data===============================
        createDBHashMap(Constant.TRANSMISSION,DBUtilsNew.DBTableType.REQUEST_TABLE, new String[]{"u_index", "u_type", "u_url", "u_state"});
        createDBHashMap(Constant.AUTH, DBUtilsNew.DBTableType.AUTH_TABLE ,new String[]{"userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" });
        //==============================================Delete Need
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    //abstract Function
    @Override
    public void ProcessDBResult() {
        //Read 2 DB
    }
    //

    //TestFragment Interface Saction==================================
    @Override
    public void Request(DBUtilsNew.DBTransaction[] mTranSaction) {
        for(int i = 0;i<mTranSaction.length;i++)
        {
            DBTableHashMap.get(mTranSaction[i].DBTableID).Request(mTranSaction);
        }
    }

    @Override
    public void Request(DBUtilsNew.DBTransaction data) {
        //ignore
    }

    @Override
    public void ResponseConTactData(ArrayList<HashMap<String, String>> data, int TableID) {
        //if Check MaxCount Data in
        if(responseCount >= DBTableHashMap.size())
        {
            responseCount = 0;
            ProcessDBResult();
        }
        else if(responseCount < DBTableHashMap.size())
        {
            switch (TableID)
            {
                case DBUtilsNew.DBTableType.REQUEST_TABLE:

                    break;

                case DBUtilsNew.DBTableType.AUTH_TABLE:

                    break;
            }
            responseCount++;
        }

    }
    //only Test Read DB========================================================Delete Need
    public void createDBHashMap(String pDBTableName,Integer DBTableName, String[] pDBColumName)
    {
        DBUtilsNew DBUtilsMake = new DBUtilsNew();
        DBUtilsMake.setDB(pDBTableName,pDBColumName);
        DBTableHashMap.put(DBTableName,DBUtilsMake);
        DBTableHashMap.get(DBTableName).mTableType = DBTableName;
        DBTableHashMap.get(DBTableName).setmParentInterface(this);//this interface
    }
    //===========================================================================================
}
