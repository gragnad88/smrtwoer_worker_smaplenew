package com.samwoo.sampleworker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtilsLocalService;
import com.samwoo.sampleworker.DB.LocalService;
import com.samwoo.sampleworker.Service.BackGroundService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static com.samwoo.sampleworker.DB.LocalService.MSG_REQUEST_TRANSACTION;

public class MainActivityOld extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl{
    SmartWorkApplication mApp;

    private static final int CAMERA_REQUEST_CODE = 2320;
    private static final int VIDEO_REQUEST_CODE = 2321;
    private static final int LOGOUT_FINISH = 7000;

    TextView workerName, workerNumber;
    //
    boolean onceTimeDialog = false;
    //
    Handler mHandler = null;
    boolean isThreadStart = false;
    String gMData = "[No Barcode]";

    String type = null;

    String fileName;
    private final String rootSD = Environment.getExternalStorageDirectory().toString();

    String save_Path;
    String save_folder;
    MainActivityOld.DownloadThread dThread;

    private MediaPlayer player;
    private VideoControllerView controller;
    FrameLayout mFrameLayout;
    FrameLayout mVideoCaptureLayout;
    SurfaceHolder mVideoHolder;

    AlertDialog dialog;
    AlertDialog loadDialog;
    ProgressDialog mentoringDialog;

    //    DBUtils qr_code = null /*new DBUtils()*/;
    DBUtilsLocalService request = null /*new DBUtils()*/;
    DBUtilsLocalService auth = null /*new DBUtils()*/;
//    DBUtils checklist = null;

    ArrayList<HashMap<String, String>> qrCodeContact = new ArrayList<>();
    ArrayList<HashMap<String, String>> requestContact = new ArrayList<>();
    ArrayList<HashMap<String, String>> authContact = new ArrayList<>();
    ArrayList<HashMap<String, String>> checkListContact = new ArrayList<>();
    ArrayList<HashMap<String, String>> request_list = new ArrayList<>();
    HashMap<String, String> request_id = new HashMap<>();
    String request_index;

    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8;
    Button captureTransBtn, captureSaveBtn, recaptureBtn;
    WebView logo_img;
    ImageView captureImage;
    SurfaceView captureVideo;
    LinearLayout video_worker, captureLayout;
    ListView video_worker_list;
    boolean isCall;

    long mentoringRequestTime;

    Handler backgroundThreadWorkerHandler = null;
    boolean isDownload = false;
    boolean mRequestCompleted = false;
    boolean mAuthCompleted = false;
    boolean mQRCodeCompleted = false;
    boolean mChecklistCompleted = false;
    long startMills = 0;
    long targetMills = 0;
    long sleepMills = 0;
    //LocalService add Data==========================================================Start
    LocalService mService = null;
    Messenger msgService = null;
    final Messenger mMessenger = new Messenger(new MainActivityOld.IncomingHandler());
    Thread DBQueryWorkerThread;
    BlockingQueue<DBUtilsLocalService.QueryResultData> mQueryResultQueue;
    //Service Connecting
    private class ConnectionLocalService implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i("+Bind:", "launch");
            LocalService.LocalBinder binder = (LocalService.LocalBinder) service;
            mService = binder.getService();

            msgService = binder.getMessenger();

            setServiceMessenger();

            mainActivityBindFunc();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            msgService = null;
        }
    }

    // Set Service Messenger
    void setServiceMessenger() {
        try {
            Message msg = Message.obtain(null, LocalService.MSG_REGISTER_CLIENT);
            msg.replyTo = mMessenger;
            msg.obj = 4;
            msgService.send(msg);
        } catch (RemoteException e) {
            Log.e("Error : ", e.getMessage());
        }
    }

    //Start Bind Activity Service Create Add DB
    void mainActivityBindFunc() {
        try {
            Message msg = Message.obtain(null, LocalService.MSG_ADD_DB);
            msg.obj = DBUtilsLocalService.DBCreateDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, Constant.TRANSMISSION, new String[]{"u_index", "u_type", "u_url", "u_state"});
            msgService.send(msg);

            msg = Message.obtain(null, LocalService.MSG_ADD_DB);
            msg.obj = DBUtilsLocalService.DBCreateDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, Constant.AUTH, new String[]{"userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" });
            msgService.send(msg);

            msg = Message.obtain(null, LocalService.MSG_ADD_DB);
            msg.obj = DBUtilsLocalService.DBCreateDataMakeFunc(DBUtilsLocalService.DBTableType.QR_CODE_TABLE, Constant.QR_CODE, new String[]{"data" });
            msgService.send(msg);

            msg = Message.obtain(null, LocalService.MSG_ADD_DB);
            msg.obj = DBUtilsLocalService.DBCreateDataMakeFunc(DBUtilsLocalService.DBTableType.CHECKLIST_READ, Constant.CHECKLIST, new String[]{ "name", "date", "worker", "capture" });
            msgService.send(msg);
            //mIsStart = true;
        } catch (RemoteException e) {
            Log.i("launch:", " Bind" + e.getMessage());
        }
    }

    //Basic Read Messenger Data Function
    void messageReadSendCreate(Integer callReadTable) {
        Log.i("Error : ","Create: " + callReadTable);
        try {
            Log.i("Error : ","TranSaction Start Table ID: " + callReadTable);
            Message clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
            clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(callReadTable, DBUtilsLocalService.TransactionType.READ, null, null);
            msgService.send(clientMsg);
        } catch (RemoteException e) {
            Log.i("Error : ", e.getMessage());
        }
    }

    //DB InHandler Data
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DBUtilsLocalService.READY_COMPLETED:
                    DBQueryWorkerThread = new Thread(DBQueryManager);
                    DBQueryWorkerThread.start();
                    break;
                case DBUtilsLocalService.DBT_TRANSACTION_RESULT:
                    DBUtilsLocalService.QueryResultData data = (DBUtilsLocalService.QueryResultData)msg.obj;
                    try {
                        mQueryResultQueue.put(data);
                    } catch (InterruptedException e) {

                    }
                    break;
            }
        }
    }

    //DB Query Manager if in request Queue Data
    private final Runnable DBQueryManager = new Runnable() {
        @Override
        public void run() {
            Log.i("launch","DBQueryManager Start");
            //
            boolean isExit = false;
            //
            LaunchWorkerThread();
            //
            while (!isExit) {
                try {
                    Log.i("Thread", "Run Check");
                    DBUtilsLocalService.QueryResultData data = mQueryResultQueue.take();
                    Log.i("Thread", "Data in");

                    switch (data.mTranSactionType) {
                        case DBUtilsLocalService.TransactionType.READ:
                            if (data.DBTableType == DBUtilsLocalService.DBTableType.REQUEST_TABLE) {
                                requestContact= data.listData;
                                mRequestCompleted = true;
                            } else if (data.DBTableType == DBUtilsLocalService.DBTableType.AUTH_TABLE) {
                                authContact = data.listData;
                                mAuthCompleted = true;
                            } else if (data.DBTableType == DBUtilsLocalService.DBTableType.QR_CODE_TABLE) {
                                qrCodeContact = data.listData;
                                mQRCodeCompleted = true;
                            } else if (data.DBTableType == DBUtilsLocalService.DBTableType.CHECKLIST_READ) {
                                checkListContact = data.listData;
                                mHandler.obtainMessage(DBUtilsLocalService.DBTableType.CHECKLIST_READ);
                                mChecklistCompleted = true;
                            } else {
                                //
                            }

                            if (mRequestCompleted && mAuthCompleted && mQRCodeCompleted && mChecklistCompleted) {
                                mRequestCompleted = false;
                                mAuthCompleted = false;
                                mQRCodeCompleted = false;
                                mChecklistCompleted = false;
                                Log.i("launch", "ProcessDB");
                                processDBQueryResult(requestContact,authContact,qrCodeContact);
                            }
                            break;
                        case DBUtilsLocalService.TransactionType.UPDATE:
                        case DBUtilsLocalService.TransactionType.DELETE:
                        case DBUtilsLocalService.TransactionType.INSERT:
                            //Update, Insert, Delete  End Retire Read
                            if (data.arg == LOGOUT_FINISH) {
                                Intent i = new Intent(getApplicationContext(), ScanLoginActivity.class);
                                startActivity(i);
                            } else if (data.arg != LOGOUT_FINISH) {
                                LaunchWorkerThread();
                            }
                            //
                            break;
                        case DBUtilsLocalService.TransactionType.TRANSACTION_EXIT:
                            isExit = true;
                            break;
                    }
                } catch (InterruptedException e) {
                    isExit = true;
                    mQueryResultQueue.clear();
                    DBQueryWorkerThread = null;
                }
            }
        }
    };

    public void LaunchWorkerThread() {
        Thread t = new Thread(BackgroundRunnable);
        t.start();
    }
    //LocalService add Data==========================================================End

    private final Runnable BackgroundRunnable = new Runnable() {
        @Override
        public void run() {
            sleepMills = 0;

            if (startMills == 0) {
                startMills = System.currentTimeMillis();
                targetMills = startMills + 1000;

                sleepMills = 1000;

            } else {
                long curMills = System.currentTimeMillis();
                sleepMills = targetMills - curMills;

                if (sleepMills > 0) {
                    targetMills += 1000;
                } else {
                    startMills = System.currentTimeMillis();
                    targetMills = startMills + 1000;
                    sleepMills = 0;
                }
            }

            try {
                Thread.sleep(sleepMills);
                messageReadSendCreate(DBUtilsLocalService.DBTableType.REQUEST_TABLE);
                messageReadSendCreate(DBUtilsLocalService.DBTableType.AUTH_TABLE);
                messageReadSendCreate(DBUtilsLocalService.DBTableType.QR_CODE_TABLE);
                messageReadSendCreate(DBUtilsLocalService.DBTableType.CHECKLIST_READ);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };

    String downloadType;

    @Override
    protected void onStart() {
        isCall = false;

        // We need to change the following code at onStart()!!!
        mRequestCompleted = false;
        mAuthCompleted = false;

        isDownload = false;

        //Thread t = new Thread(BackgroundRunnable); //+commented by swseo
        //t.start(); //+commented by swseo

        @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("MM월dd일 HH시mm분ss초").format(new Date());
        auth.update(true, mApp.getAuthID(), new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "메인", timestamp });

        super.onStart();
    }

    boolean mIsStart = false;

    @SuppressLint({"HandlerLeak", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main);

        mApp = (SmartWorkApplication)getApplication();

        mQueryResultQueue = new ArrayBlockingQueue<>(20);

        request = new DBUtilsLocalService(DBUtilsLocalService.DBTableType.REQUEST_TABLE,mQueryResultQueue);
        auth = new DBUtilsLocalService(DBUtilsLocalService.DBTableType.AUTH_TABLE,mQueryResultQueue);
//        qr_code = new DBUtils(DBTableType.QR_CODE_READ,mQueryResultQueue);
//
//        qr_code.setDB(this, Constant.QR_CODE, new String[]{ "data" },true);
        request.setDB(this, Constant.TRANSMISSION, new String[]{ "u_index", "u_type", "u_url", "u_state" },true);
        auth.setDB(this, Constant.AUTH, new String[]{ "userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" },true);

        logo_img = (WebView)findViewById(R.id.logo);
        btn1 = (Button)findViewById(R.id.fileViewBtn);
        btn2 = (Button)findViewById(R.id.QRcodeBtn);
        btn3 = (Button)findViewById(R.id.cameraBtn);
        btn4 = (Button)findViewById(R.id.settingBtn);
        btn5 = (Button)findViewById(R.id.DBconnectBtn);
        btn6 = (Button)findViewById(R.id.ARviewBtn);
        btn7 = (Button)findViewById(R.id.videoCallBtn);
        btn8 = (Button)findViewById(R.id.logoutBtn);
        captureSaveBtn = (Button)findViewById(R.id.captureSaveBtn);
        captureTransBtn = (Button)findViewById(R.id.captureTransBtn);
        recaptureBtn = (Button)findViewById(R.id.recaptureBtn);
        captureLayout = (LinearLayout)findViewById(R.id.captureLayout);
        video_worker = (LinearLayout)findViewById(R.id.workerLayout);
        captureImage = (ImageView)findViewById(R.id.capture_image);
        video_worker_list = (ListView)findViewById(R.id.workerList);
        workerName = (TextView)findViewById(R.id.workerName);
        workerNumber = (TextView)findViewById(R.id.workerNumber);
        mFrameLayout = (FrameLayout)findViewById(R.id.mainFrameLayout);

        int width = 153;
        String url = Constant.FILE_PATH  + mApp.getManager() + "_logo.png";

        String data = "<html><head><title>Example</title><meta name=\"viewport\"\"content=\"width=" + width + ", initial-scale=0.65 \" /></head>";
        data = data + "<body><center><img width=\"" + width + "\" src=\"" + url + "\" /></center></body></html>";

        logo_img.loadData(data, "text/html", null);
        workerName.setText("사원 이름: " + mApp.getWorkerName());
        workerNumber.setText("사원 번호: " + mApp.getAuthID());

        isThreadStart = true;
        isCall = false;

        mentoringRequestTime = 0;

        String ext = Environment.getExternalStorageState();
        if (ext.equals(Environment.MEDIA_MOUNTED)) {
            save_Path = Environment.getExternalStorageDirectory().getAbsolutePath();
        }

        request_index = mApp.getAuthID();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what < 3) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityOld.this);
                    String title = null;

                    ResetUnbind();

                    switch (msg.what) {
                        case 0:
                            type = "image";
                            title = "이미지";
                            save_folder = "/Pictures";
                            break;
                        case 1:
                            type = "video";
                            title = "동영상";
                            save_folder = "/Movies";
                            break;
                        case 2:
                            type = "text";
                            title = "문서";
                            save_folder = "/Documents";
                            break;
                    }

                    downloadType = type;

                    builder.setTitle(title)
                            .setMessage("파일이 수신되었습니다")
                            .setPositiveButton("보기", (dialog, which) -> {
                                if (!isDownload) {
                                    isDownload = true;
                                    mApp.setControlVideo(false);
                                    //
                                    request.update(true,request_id.get(downloadType), new String[]{ mApp.getAuthID(), downloadType, "", "Accept" });
                                    //
                                    BindInit();
                                    fileDownload();
                                }
                            })
                            .setNegativeButton("닫기", (dialog, which) -> {
                                //
                                request.update(true,request_id.get(downloadType), new String[]{ mApp.getAuthID(), downloadType, "", "Cancel" });
                                //
                                BindInit();
                            });
                    builder.setCancelable(false);

                    dialog = builder.create();
                    if (!dialog.isShowing()) {
                        dialog.show();
                    }
                } else if (msg.what == 3) {
                    mApp.setControlVideo(true);
                    for (int i = 0; i < qrCodeContact.size(); i++) {
                        if (gMData.equals(qrCodeContact.get(i).get("data"))) {
                            mApp.setUrl(qrCodeContact.get(i).get("data"));
                            intDataSwitch();
                            gMData = "[No Barcode]";
                            break;
                        }

                        if (i + 1 == qrCodeContact.size()) {
                            Toast.makeText(MainActivityOld.this, "등록된 정보가 없습니다. 확인 후 다시 인식해 주세요.", Toast.LENGTH_LONG).show();
                        }
                    }
                } else if (msg.what == 6) {
                    isDownload = false;

                    // TODO Auto-generated method stub
                    // 파일 다운로드 종료 후 다운받은 파일을 실행시킨다.
                    showDownloadFile();
                } else if (msg.what == 7) {
                    // 방 만드는 시간을 위해 대기
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mentoringDialog.dismiss();
                    Intent i = new Intent(MainActivityOld.this, MentoringActivity.class);
                    startActivity(i);
                } else if (msg.what == 8) {
                    isCall = true;
                    Intent i = new Intent(MainActivityOld.this, MentoringActivity.class);
                    startActivity(i);
                } else if (msg.what == DBUtilsLocalService.DBTableType.CHECKLIST_READ) {
                    for (int i = 0; i < checkListContact.size(); i++) {
                        if (Objects.requireNonNull(checkListContact.get(i).get("name")).equals(mApp.getUrl())) {
                            mApp.setChecklist(checkListContact.get(i).get("id"));
                        }
                    }
                    Intent intent = new Intent(getApplicationContext(), CheckListInfoActivity.class);
                    startActivity(intent);
                } else if (msg.what == LOGOUT_FINISH) {
                    Intent i = new Intent(getApplicationContext(), ScanLoginActivity.class);
                    startActivity(i);
                }
            }
        };
    }

    public void mOnClick(View v) {
        Intent i;
        @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("MM월dd일 HH시mm분ss초").format(new Date());
        Message clientMsg;
        switch (v.getId()) {
            // 내 파일
            case R.id.fileViewBtn:
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "내 파일", timestamp }, "");
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
//                //핸드폰 용
//                i = new Intent(this, FileMenu.class);
//                startActivityForResult(i, FILE_REQUEST_CODE);

                //리얼웨어 용
                i = getPackageManager().getLaunchIntentForPackage("com.realwear.filebrowser");
                assert i != null;
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            // 정보 인식
            case R.id.QRcodeBtn:
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "정보 인식", timestamp }, "");
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                new IntentIntegrator(this).initiateScan();
                break;
            // 촬영
            case R.id.cameraBtn:
                selectCapture();
                break;
            // 공정 관리
            case R.id.DBconnectBtn:
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "공정 관리", timestamp }, "");
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                i = new Intent(this, CheckListItemActivity.class);
                startActivity(i);
                break;
            // AR 매뉴얼
            case R.id.ARviewBtn:
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "AR 매뉴얼", timestamp }, "");
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                i = getPackageManager().getLaunchIntentForPackage("com.Samwooim.RealWearMEclient");
//                i = getPackageManager().getLaunchIntentForPackage("com.SamwooIM.KoreaTireDemo");
                assert i != null;
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            // 환경설정
            case R.id.settingBtn:
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "환경설정", timestamp }, "");

                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                i = getPackageManager().getLaunchIntentForPackage("com.android.settings");
                assert i != null;
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            // 원격 멘토링
            case R.id.videoCallBtn:
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "원격 멘토링", timestamp }, "");
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                @SuppressLint("SimpleDateFormat") String roomTime = new SimpleDateFormat("yyMMddHHmmssSS").format(new Date());

                isCall = true;

                mApp.setRoomNumber(roomTime);

                String id = request_id.get("videoCall");

                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.UPDATE, id,
                        new String[]{ request_index, "videoCall", mApp.getRoomNumber(), "Request" }, "");
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                mentoringDialog = new ProgressDialog(this);
                mentoringDialog.setMessage("원격 멘토링 요청 중...");
                mentoringDialog.setCancelable(true);
                mentoringDialog.show();

                mentoringRequestTime = System.currentTimeMillis();

                break;
            case R.id.logoutBtn:
                //Logout no Bind Update
                clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                        new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), "", "", "" }, LOGOUT_FINISH);
                try {
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    public void processDBQueryResult(ArrayList<HashMap<String, String>> requestContact, ArrayList<HashMap<String, String>> authContact, ArrayList<HashMap<String, String>> qrCodeContact) {
        request_list.clear();
        request_id.clear();

        JSONArray objList = new JSONArray();
        if (objList.length() != 0 ) for (int i = 0; i < objList.length(); i++) objList.remove(i);

        for (int i = 0; i < requestContact.size(); i++) {
            if (Objects.requireNonNull(requestContact.get(i).get("u_index")).equals(request_index)) {
                HashMap<String, String> list = new HashMap<>();

                list.put("id", requestContact.get(i).get("id"));
                list.put("u_index", requestContact.get(i).get("u_index"));
                list.put("u_type", requestContact.get(i).get("u_type"));
                list.put("u_url", requestContact.get(i).get("u_url"));
                list.put("u_state", requestContact.get(i).get("u_state"));

                request_id.put(requestContact.get(i).get("u_type"), requestContact.get(i).get("id"));

                request_list.add(list);
            }
        }

        Iterator<HashMap<String, String>> itr = request_list.iterator();
        int updateLevel = 0;
        while (itr.hasNext()) {
            HashMap<String, String> obj = itr.next();

            Message msg;
            if (obj.get("u_state").equals("Transmission")) {
                mApp.setUrl(obj.get("u_url"));

                switch (obj.get("u_type")) {
                    case "image":
                        objList.put(DB_reset(obj, true));
                        msg = Message.obtain(null, 0);
                        msg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.UPDATE, objList, null);
                        mHandler.sendMessage(msg);
                        updateLevel = 0;
                        break;
                    case "video":
                        objList.put(DB_reset(obj, true));
                        msg = Message.obtain(null, 1);
                        msg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.UPDATE, objList, null);
                        mHandler.sendMessage(msg);
                        updateLevel = 1;
                        break;
                    case "text":
                        objList.put(DB_reset(obj, true));
                        msg = Message.obtain(null, 2);
                        msg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.UPDATE, objList, null);
                        mHandler.sendMessage(msg);
                        updateLevel = 2;
                        break;
                }
            } else if (obj.get("u_state").equals("Response")) {
                if (obj.get("u_type").equals("videoCall")) {
                    if(mentoringDialog != null) {
                        if (mentoringDialog.isShowing()) {
                            objList.put(DB_reset(obj, false));

                            mHandler.sendEmptyMessage(7);
                            updateLevel = 7;
                        }
                        else {
                            // recall 에 대한 코딩
                        }
                    }
                }
            }

            if (mentoringRequestTime != 0) {
                long mentoringTimeOver = System.currentTimeMillis();
                if (mentoringTimeOver - mentoringRequestTime >= 60 * 1000) {
                    if (mentoringDialog.isShowing()) {
                        try {
                            Message clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                            clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.UPDATE, request_id.get("videoCall"),
                                    new String[]{ obj.get("u_index"), "videoCall", "", "Missed" }, "");
                            msgService.send(clientMsg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        mentoringDialog.dismiss();
                        mentoringRequestTime = 0;
                        updateLevel = 0;
                    }
                }
            }
        }

        if (objList.length() != 0) {
            if (updateLevel > 3) {
                try {
                    Message clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                    clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.UPDATE, objList, null);
                    msgService.send(clientMsg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        } else {
            LaunchWorkerThread();
        }
    }

    private JSONObject DB_reset(HashMap<String, String> hashObj, boolean type) {
        String id = hashObj.get("id");
        JSONObject obj = new JSONObject();

        try {
            obj.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (type) {
            if (Objects.requireNonNull(hashObj.get("u_type")).equals("image")) this.type = "image";
            else if (Objects.requireNonNull(hashObj.get("u_type")).equals("video")) this.type = "video";
            else if (Objects.requireNonNull(hashObj.get("u_type")).equals("text")) this.type = "text";
        }

        return obj;
    }

    private class DownloadThread extends Thread {
        String ServerUrl;
        String LocalPath;

        DownloadThread(String serverPath, String localPath) {
            ServerUrl = serverPath;
            LocalPath = localPath;
        }

        @Override
        public void run() {
            URL imgurl;
            int Read;
            try {
                imgurl = new URL(ServerUrl);
                HttpURLConnection conn = (HttpURLConnection) imgurl.openConnection();
                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];
                InputStream is = conn.getInputStream();
                File file = new File(LocalPath);
                FileOutputStream fos = new FileOutputStream(file);

                for (;;) {
                    Read = is.read(tmpByte);
                    if (Read <= 0) break;
                    fos.write(tmpByte, 0, Read);
                }

                is.close();
                fos.close();
                conn.disconnect();
            } catch (MalformedURLException e) {
                Log.e("ERROR1", e.getMessage());
            } catch (IOException e) {
                Log.e("ERROR2", e.getMessage());
                e.printStackTrace();
            }
            mHandler.sendEmptyMessage(6);
        }
    }

    private void showDownloadFile() {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        File file = new File(save_Path + save_folder + "/" + mApp.getUrl());

        // 파일 확장자 별로 mime type 지정해 준다.
        if (mApp.getUrl().endsWith("mp3")) {
            intent.setDataAndType(Uri.fromFile(file), "audio/*");
        } else if (mApp.getUrl().endsWith("mp4")) {
            intent = new Intent(this, ReceptionVideoPlayerActivity.class);
        } else if (mApp.getUrl().endsWith("jpg") || mApp.getUrl().endsWith("jpeg")
                || mApp.getUrl().endsWith("JPG") || mApp.getUrl().endsWith("gif")
                || mApp.getUrl().endsWith("png") || mApp.getUrl().endsWith("bmp")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        } else if (mApp.getUrl().endsWith("txt")) {
            intent.setDataAndType(Uri.fromFile(file), "text/*");
        } else if (mApp.getUrl().endsWith("doc") || mApp.getUrl().endsWith("docx")) {
            intent.setDataAndType(Uri.fromFile(file), "application/msword");
        } else if (mApp.getUrl().endsWith("xls") || mApp.getUrl().endsWith("xlsx")) {
            intent.setDataAndType(Uri.fromFile(file),
                    "application/vnd.ms-excel");
        } else if (mApp.getUrl().endsWith("ppt") || mApp.getUrl().endsWith("pptx")) {
            intent.setDataAndType(Uri.fromFile(file),
                    "application/vnd.ms-powerpoint");
        } else if (mApp.getUrl().endsWith("pdf")) {
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        }
        startActivity(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE) { // 2320
                showCapture("image");
            } else if (requestCode == VIDEO_REQUEST_CODE) { // 2321
                // 비디오 저장
                showCapture("video");
            } else if (result.getFormatName().equals(IntentIntegrator.QR_CODE)) {
                gMData = result.getContents();
                mHandler.sendEmptyMessage(3);
            }
        }
    }

    void intDataSwitch() {
        if (mApp.getUrl().endsWith("mp4") || mApp.getUrl().endsWith("avi")) {
            save_folder = "/Movies";
            fileDownload();
        } else if (mApp.getUrl().endsWith("jpg") || mApp.getUrl().endsWith("jpeg")
                || mApp.getUrl().endsWith("JPG") || mApp.getUrl().endsWith("gif")
                || mApp.getUrl().endsWith("png") || mApp.getUrl().endsWith("bmp")) {
            save_folder = "/Pictures";
            fileDownload();
        } else if (mApp.getUrl().endsWith("txt") || mApp.getUrl().endsWith("doc")
                || mApp.getUrl().endsWith("docx") || mApp.getUrl().endsWith("xls")
                || mApp.getUrl().endsWith("xlsx") || mApp.getUrl().endsWith("ppt")
                || mApp.getUrl().endsWith("pptx") || mApp.getUrl().endsWith("pdf")) {
            save_folder = "/Documents";
            fileDownload();
        } else {
//            checklist = new DBUtils();
//            checklist.setDB(this, Constant.CHECKLIST, new String[]{ "name", "date", "worker", "capture" });
//            checklist.read(DBTableType.CHECKLIST_READ, mHandler);

        }
    }

    public void fileDownload() {
        File dir = new File(save_Path + save_folder);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (!new File(save_Path + save_folder + "/" + mApp.getUrl()).exists()) {
            dThread = new MainActivityOld.DownloadThread(Constant.FILE_PATH + "/" + mApp.getUrl(),
                    save_Path + save_folder + "/" + mApp.getUrl());
            dThread.start();
        } else mHandler.sendEmptyMessage(6);
    }

    @SuppressLint("LongLogTag")
    public void uploadFile(final String sourceFileUri) {
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "********";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1024 * 1024;

        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {
            loadDialog.dismiss();
            Log.e("uploadFile", "Source File not exist :" + "Pictures/" + sourceFileUri);
        } else {
            try {
                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Constant.FILE_PATH_PHP);

                // Open a HTTP  connection to  the URL
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod(Constant.POST_METHOD);
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", sourceFileUri);

                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"");
                dos.write(sourceFileUri.getBytes("utf-8"));
                dos.writeBytes("\"" + lineEnd);
                dos.writeBytes(lineEnd);

//                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + sourceFileUri + "\"" + lineEnd);
//                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);

                buffer = new byte[bufferSize];
                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                int serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

                if(serverResponseCode == 200) {
                    runOnUiThread(() -> Toast.makeText(MainActivityOld.this, "File Upload Complete.", Toast.LENGTH_SHORT).show());
                    Message clienMsg = Message.obtain(null,MSG_REQUEST_TRANSACTION);
                    clienMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE,DBUtilsLocalService.TransactionType.UPDATE,request_id.get("capture"), new String[]{ request_index, "capture", fileName, "Transmission" },"" );
                    try {
                        msgService.send(clienMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    //
//                    request.update(request_id.get("capture"), new String[]{ request_index, "capture", fileName, "Transmission" });
                    //
                }
                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                loadDialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(() -> Toast.makeText(MainActivityOld.this, "MalformedURLException", Toast.LENGTH_SHORT).show());

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                loadDialog.dismiss();
                e.printStackTrace();

                runOnUiThread(() -> Toast.makeText(MainActivityOld.this, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show());
                Log.e("Upload file to server Exception", "Exception : " + e.getMessage(), e);
            }
            loadDialog.dismiss();
        }
    }


    public void selectCapture() {
        @SuppressLint("SimpleDateFormat") String captureTime = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
        @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("MM월dd일 HH시mm분ss초").format(new Date());

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityOld.this);
        builder.setTitle("촬영")
                .setMessage("타입을 선택해 주세요.")
                .setPositiveButton("사진", (dialog, which) -> {

                    Message clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
                    clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                            new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "사진 촬영", timestamp }, "");
                    try {
                        msgService.send(clientMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    fileName = captureTime + "_" + mApp.getWorkerName() + ".png";

                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File file = new File(save_Path + "/Pictures", fileName);

                    Uri outputFileUri;
                    outputFileUri = Uri.fromFile(file);

                    i.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                    startActivityForResult(i, CAMERA_REQUEST_CODE);
                })
                .setNegativeButton("동영상", (dialog, which) -> {

                    Message clientMsg = Message.obtain(null,MSG_REQUEST_TRANSACTION);
                    clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                            new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "동영상 촬영", timestamp }, "");
                    try {
                        msgService.send(clientMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    fileName = captureTime + "_" + mApp.getWorkerName() + ".mp4";

                    Intent i = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                    File video = new File(save_Path + "/Movies", fileName);

                    Uri outputVideoUri;
                    outputVideoUri = Uri.fromFile(video);

                    i.putExtra(MediaStore.EXTRA_OUTPUT, outputVideoUri);

                    startActivityForResult(i, VIDEO_REQUEST_CODE);
                })
                .setNeutralButton("취소", (dialog, which) -> { });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public void showCapture(String type) {
        // 캡쳐된 사진, 동영상이 기기에 저장되어 스캔할 시간을 주기위해서
        // sleep 을 주지않으면 사진, 동영상이 뜨지 않을 때가 있음
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setButtonClicked(false);

        if(type.equals("image")) {
            captureLayout.setVisibility(View.VISIBLE);

            File file = new File(save_Path + "/Pictures", fileName);

            if(file.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

                captureImage.setImageBitmap(myBitmap);
            }

            captureSaveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    captureLayout.setVisibility(View.GONE);
                    setButtonClicked(true);

                    Toast.makeText(getApplicationContext(), "사진이 저장되었습니다.", Toast.LENGTH_SHORT).show();
                }
            });

            captureTransBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    captureLayout.setVisibility(View.GONE);
                    setButtonClicked(true);

                    loadDialog = ProgressDialog.show(MainActivityOld.this, "", "Uploading File...", true);

                    new Thread(() -> uploadFile(rootSD + "/Pictures/" + fileName)).start();
                }
            });

            recaptureBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    @SuppressLint("SimpleDateFormat") String captureTime = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
                    @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("MM월dd일 HH시mm분ss초").format(new Date());

                    captureLayout.setVisibility(View.GONE);
                    setButtonClicked(true);

                    File deleteFile = new File(save_Path + "/Pictures", fileName);
                    deleteFile.delete();

                    Message clientMsg = Message.obtain(null,MSG_REQUEST_TRANSACTION);
                    clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                            new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "사진 촬영", timestamp }, "");
                    try {
                        msgService.send(clientMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    fileName = captureTime + "_" + mApp.getWorkerName() + ".png";

                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File file = new File(save_Path + "/Pictures", fileName);

                    Uri outputFileUri;
                    outputFileUri = Uri.fromFile(file);

                    i.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                    startActivityForResult(i, CAMERA_REQUEST_CODE);
                }
            });
        } else if(type.equals("video")) {
            captureLayout.setVisibility(View.GONE);

            File file = new File(save_Path + "/Movies", fileName);

            if(file.exists()) {
                //
                makeCaptureView();
            }
        }
    }

    Intent mLocalServiceintent;
    MainActivityOld.ConnectionLocalService mConnectionLocalService;
    void BindInit()
    {
        if(DBQueryWorkerThread == null)
        {
            targetMills = 0;
            mLocalServiceintent = new Intent(this, LocalService.class);
            mConnectionLocalService = new MainActivityOld.ConnectionLocalService();
            bindService(mLocalServiceintent, mConnectionLocalService, Context.BIND_AUTO_CREATE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //
        mQueryResultQueue = new ArrayBlockingQueue<>(20);
        onceTimeDialog = false;
        Log.i("MainActivityOld","+Resume" );
        //
        Intent extraIntent =new Intent(this, BackGroundService.class);
        stopService(extraIntent);
        //
        //BindInit();
        //
        targetMills = 0;

        mLocalServiceintent = new Intent(this, com.samwoo.sampleworker.DB.LocalService.class);
        mConnectionLocalService = new MainActivityOld.ConnectionLocalService();
        bindService(mLocalServiceintent, mConnectionLocalService, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        ResetUnbind();
        Log.i("MainActivityOld","+Pause" );
        super.onPause();
    }

    @Override
    protected void onStop() {

        Log.i("MainActivityOld","+Stop" );
        super.onStop();
    }

    @Override
    protected void onUserLeaveHint() {
        Log.i("MainActivityOld","+Exit" );
        Intent extraIntent =new Intent(this, BackGroundService.class);
        stopService(extraIntent);
        //Only One Check
        startService(new Intent(this, BackGroundService.class));
        super.onUserLeaveHint();
    }

    @Override
    protected void onDestroy() {
        Intent extraIntent =new Intent(this, BackGroundService.class);
        stopService(extraIntent);
        Log.i("MainActivityOld","+Destroy" );

        super.onDestroy();
    }

    void ResetUnbind() {
        DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.REQUEST_TABLE, DBUtilsLocalService.TransactionType.TRANSACTION_EXIT,null,null);

        if(DBQueryWorkerThread != null)
            DBQueryWorkerThread.interrupt();

        unbindService(mConnectionLocalService);
    }

    @Override
    public void onBackPressed() {
        mentoringRequestTime = 0;
        Log.i("MainActivityOld","+Pause" );
        Toast.makeText(this, "로그아웃을 하시려면 \"로그아웃\" 명령어를 입력하여 주세요.", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        controller.show(0);
        return false;
    }

    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        player.setDisplay(holder);
        player.prepareAsync();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }
    // End SurfaceHolder.Callback

    // Implement MediaPlayer.OnPreparedListener
    @Override
    public void onPrepared(MediaPlayer mp) {
        controller = new VideoControllerView(this);
        controller.setMediaPlayer(this);
        FrameLayout test = findViewById(R.id.videoSurfaceContainer);
//        controller.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
        controller.setAnchorView(test);
        player.start();
        controller.show(0);
    }
    // End MediaPlayer.OnPreparedListener

    // Implement VideoMediaController.MediaPlayerControl
    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return player.getCurrentPosition();
    }

    @Override
    public int getDuration() {
        return player.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public void pause() {
        player.pause();
    }

    @Override
    public void seekTo(int i) {
        player.seekTo(i);
    }

    @Override
    public void start() {
        player.start();
    }

    @Override
    public void trans() {
        setButtonClicked(true);

        deleteCaptureView();

        loadDialog = ProgressDialog.show(MainActivityOld.this, "", "Uploading File...", true);

        new Thread(() -> uploadFile(rootSD + "/Movies/" + fileName)).start();
    }

    @Override
    public void save() {
        setButtonClicked(true);

        deleteCaptureView();

        Toast.makeText(this, "동영상이 저장되었습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void recapture() {
        @SuppressLint("SimpleDateFormat") String captureTime = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
        @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("MM월dd일 HH시mm분ss초").format(new Date());

        File deleteFile = new File(save_Path + "/Movies", fileName);
        deleteFile.delete();

        Message clientMsg = Message.obtain(null, MSG_REQUEST_TRANSACTION);
        clientMsg.obj = DBUtilsLocalService.DBTranSactionDataMakeFunc(DBUtilsLocalService.DBTableType.AUTH_TABLE, DBUtilsLocalService.TransactionType.UPDATE, mApp.getAuthID(),
                new String[]{ mApp.getId(), mApp.getPassword(), mApp.getWorkerName(), mApp.getManager(), mApp.getSerial(), "동영상 촬영", timestamp }, "");
        try {
            msgService.send(clientMsg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        fileName = captureTime + "_" + mApp.getWorkerName() + ".mp4";

        Intent i = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        File video = new File(save_Path + "/Movies", fileName);

        Uri outputVideoUri;
        outputVideoUri = Uri.fromFile(video);

        i.putExtra(MediaStore.EXTRA_OUTPUT, outputVideoUri);

        startActivityForResult(i, VIDEO_REQUEST_CODE);

        setButtonClicked(true);

        deleteCaptureView();
    }
    // End VideoMediaController.MediaPlayerControl

    public void setButtonClicked(boolean clickable) {
        btn1.setClickable(clickable);
        btn2.setClickable(clickable);
        btn3.setClickable(clickable);
        btn4.setClickable(clickable);
        btn5.setClickable(clickable);
        btn6.setClickable(clickable);
        btn8.setClickable(clickable);
        btn7.setClickable(clickable);
    }


    public void makeCaptureView() {
//        captureVideo = (SurfaceView)findViewById(R.id.videoSurface);
        LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mVideoCaptureLayout = (FrameLayout) mInflater.inflate(R.layout.video_captrue_view, null);
        captureVideo = (SurfaceView) mVideoCaptureLayout.findViewById(R.id.videoSurface);
        mFrameLayout.addView(mVideoCaptureLayout);

        mVideoHolder = captureVideo.getHolder();
        mVideoHolder.addCallback(MainActivityOld.this);//////////////////////////////////////////

        try {
            player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(save_Path + "/Movies/" + fileName);
            player.setOnPreparedListener(this);//////////////////////////////////////////////////////////
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    controller.show(0);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteCaptureView() {
        mVideoHolder.removeCallback(MainActivityOld.this);
        controller.removeAllViews(); // Remove MediaControllerView
        controller.hide(); // Remove All Views of AnchorView
        player.release();
        mFrameLayout.removeView(mVideoCaptureLayout);
    }
}
