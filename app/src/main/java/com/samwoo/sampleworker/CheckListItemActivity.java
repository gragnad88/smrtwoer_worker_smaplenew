package com.samwoo.sampleworker;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;

public class CheckListItemActivity extends AppCompatActivity {
    SmartWorkApplication mApp;

    DBUtils checklist = new DBUtils();

    Handler mHandler = null;

    WebView logo_img;
    TextView workerName, workerNumber;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_check_list_item);

        mApp = (SmartWorkApplication)getApplication();

        logo_img = (WebView)findViewById(R.id.logo);
        workerName = (TextView)findViewById(R.id.workerName);
        workerNumber = (TextView)findViewById(R.id.workerNumber);

        int width = 153;
        String url = Constant.FILE_PATH + mApp.getManager() + "_logo.png";

        String data = "<html><head><title>Example</title><meta name=\"viewport\"\"content=\"width=" + width + ", initial-scale=0.65 \" /></head>";
        data = data + "<body><center><img width=\"" + width + "\" src=\"" + url + "\" /></center></body></html>";

        logo_img.loadData(data, "text/html", null);
        workerName.setText("사원 이름: " + mApp.getWorkerName());
        workerNumber.setText("사원 번호: " + mApp.getAuthID());

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case DBUtils.DBTableType.CHECKLIST_READ:
                        ListView listView = (ListView)findViewById(R.id.workList);

                        ListAdapter adapter = new SimpleAdapter(
                                CheckListItemActivity.this, checklist.getContactList(), R.layout.check_list,
                                new String[]{"name", "date", "worker"},
                                new int[]{R.id.name, R.id.date, R.id.worker});

                        listView.setAdapter(adapter);

                        listView.setOnItemClickListener((parent, view, position, id) -> {
                            mApp.setChecklist(checklist.getContactList().get(position).get("id"));
                            mApp.setEquipment(checklist.getContactList().get(position).get("name"));

                            Intent i = new Intent(getApplicationContext(), CheckListInfoActivity.class);
                            startActivity(i);
                        });
                        break;
                }
            }
        };

        checklist.setDB(this, Constant.CHECKLIST, new String[]{ "name", "date", "worker", "capture" });
        checklist.read(DBUtils.DBTableType.CHECKLIST_READ, mHandler);
    }
}