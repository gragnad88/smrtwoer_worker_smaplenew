package com.samwoo.sampleworker.DB;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;


import org.json.JSONArray;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class LocalService extends Service {
    //Keeps track of all current registered clients
    //Holds last Value set by a client
    Messenger mClient;

    /*Command to the service to register a client, receiving callBacks form the service. The Message replyTo
    field must be a Messenger of the client where callbacks should be sent
    */
    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_REQUEST_TRANSACTION = 3;
    public static final int MSG_ADD_DB = 4;

    int MaxTableNumber;
    // Binder given to clients
    private final IBinder binder = new LocalBinder();

    BlockingQueue<DBUtilsLocalService.QueryResultData> mServiceBlockQueue;
    HashMap<Integer,DBUtilsLocalService> mDBUtilHashMap;
    QueDataGetThread mQueueDataThread;

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.i("launch:", msg.toString());
            switch(msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClient = msg.replyTo;
                    mDBUtilHashMap.clear();
                    MaxTableNumber = (int)msg.obj;
                    Log.i("launch", "+mClientSet");
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClient = null;
                    break;
                case MSG_ADD_DB:
                    DBUtilsLocalService.DBcreateNeed mDBCreateInfo = (DBUtilsLocalService.DBcreateNeed)msg.obj;
                    DBUtilsLocalService mDBUtils = new DBUtilsLocalService(mDBCreateInfo.DBKeyValue, mServiceBlockQueue);
                    mDBUtils.setDB(mDBCreateInfo.DBTable, mDBCreateInfo.DBColum);
                    mDBUtilHashMap.put(mDBCreateInfo.DBKeyValue, mDBUtils);

                    if (mDBUtilHashMap.size() == MaxTableNumber) {
                        try {
                            Message startMsg = Message.obtain(null, DBUtilsLocalService.READY_COMPLETED);
                            mClient.send(startMsg);
                        } catch (RemoteException e) {
                            Log.i("launch", e.getMessage());
                        }
                    }
                    Log.i("launch", "Add_DB");
                    break;
                case MSG_REQUEST_TRANSACTION:
                    Log.i("launch", "Transaction Start");

                    DBUtilsLocalService.DBTransaction TransactionInfo = (DBUtilsLocalService.DBTransaction)msg.obj;
                    TransactionService(TransactionInfo);
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mServiceBlockQueue = new ArrayBlockingQueue<>(30);

        mDBUtilHashMap = new HashMap<>();
        mDBUtilHashMap.clear();

        mQueueDataThread = new QueDataGetThread();
        mQueueDataThread.start();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("launch", "UnBind QueueData Thread ");
        mQueueDataThread.interrupt();
        //mDBUtilHashMap.clear();
        return super.onUnbind(intent);
    }

    public class LocalBinder extends Binder {
        public LocalService getService() {
            return LocalService.this;
        }
        public  Messenger getMessenger()
        {
            return mMessenger;
        }
    }

    public void TransactionService(DBUtilsLocalService.DBTransaction mTranSaction) {
        switch (mTranSaction.TransectionID) {
            case DBUtilsLocalService.TransactionType.INSERT:
                String[] inputData = mTranSaction.StringData;
                mDBUtilHashMap.get(mTranSaction.DBTableID).insert(true, inputData);
                break;
            case DBUtilsLocalService.TransactionType.READ:
                mDBUtilHashMap.get(mTranSaction.DBTableID).read(true);
                break;
            case DBUtilsLocalService.TransactionType.UPDATE:
                if (mTranSaction.StringData != null) {
                    String[] StringData = mTranSaction.StringData;
                    String IDdata = mTranSaction.idValue;

                    if(mTranSaction.mArg != 0) {
                        mDBUtilHashMap.get(mTranSaction.DBTableID).update(true,IDdata, StringData, mTranSaction.mArg);
                    } else if(mTranSaction.mArg == 0) {
                        mDBUtilHashMap.get(mTranSaction.DBTableID).update(true, IDdata, StringData);
                    }
                } else if (mTranSaction.TranSactionJson != null) {
                    JSONArray getObjList = mTranSaction.TranSactionJson;
                    mDBUtilHashMap.get(mTranSaction.DBTableID).update(true, getObjList);
                }
                break;
            case DBUtilsLocalService.TransactionType.DELETE:
                JSONArray DeleteJson = mTranSaction.TranSactionJson;
                mDBUtilHashMap.get(mTranSaction.DBTableID).delete(true, DeleteJson);
                break;
            case DBUtilsLocalService.TransactionType.TRANSACTION_EXIT:
                mDBUtilHashMap.get(mTranSaction.DBTableID).Exit(true);
                break;
        }
    }

    //onCreate Queue Data Message client to Activity
    class QueDataGetThread extends Thread {
        boolean isPut = false;

        QueDataGetThread() {
            isPut = true;
        }

        @Override
        public void run() {
            //super.run();
            Log.i("launch", "QueDataThread Create");
            while (isPut) {
                try {
                    Log.i("launch", "DB_get_check");

                    if (!Thread.currentThread().isInterrupted()) {
                        Message msg = Message.obtain(null, DBUtilsLocalService.DBT_TRANSACTION_RESULT);
                        msg.obj = mServiceBlockQueue.take();
                        // Pause -> Resume 일때 받아야 되는 경우도 확인
                        DBUtilsLocalService.QueryResultData data = (DBUtilsLocalService.QueryResultData) msg.obj;
                        switch (data.mTranSactionType) {
                            case DBUtilsLocalService.TransactionType.INSERT:
                            case DBUtilsLocalService.TransactionType.DELETE:
                            case DBUtilsLocalService.TransactionType.READ:
                            case DBUtilsLocalService.TransactionType.UPDATE:
                                mClient.send(msg);
                                break;
                            case DBUtilsLocalService.TransactionType.TRANSACTION_EXIT:
                                // 바인딩 상태애가 OnPause OnDestroy Send Not Check Need
                                break;
                        }
                    }
                } catch (Exception e) {
                    isPut = false;
                    //mServiceBlockQueue.clear();
                }
            }
        }
    }
}