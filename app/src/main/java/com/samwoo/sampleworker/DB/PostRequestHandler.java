package com.samwoo.sampleworker.DB;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;

import java.util.HashMap;

public class PostRequestHandler extends AsyncTask<Void,Void,String> {
    //Post방식의 Request를 보내기 위한 클래스
    //네트워트 관련 처리 -> 별도의 thread(BackgroundWorker.java)를 생성하여 수행
    private static final int JSON = 0;
    private static final int HANDLER = 1;
    private static final int RUNNABLE = 2;

    private String url;
    private HashMap<String, String> requestedParams;
    private JSONArray requestedParamsJSON;
    private boolean isJSON;

    private Handler mResponseHandler;
    private Runnable mRunnableObj;
    private int id;
    private int mType = -1;

    PostRequestHandler(String url, HashMap<String, String> params) {
        this.url = url;
        this.requestedParams = params;
        this.isJSON = false;
    }

    public PostRequestHandler(String url, JSONArray params) {
        this.url = url;
        this.requestedParamsJSON = params;
        this.isJSON = true;
        mType = JSON;
    }

    public PostRequestHandler(String url, JSONArray params, Handler handler, int id) {
        this.url = url;
        this.requestedParamsJSON = params;
        this.isJSON = true;
        this.id = id;
        mResponseHandler = handler;
        mType = HANDLER;
    }

    public PostRequestHandler(String url, JSONArray params, Handler handler, Runnable r) {
        this.url = url;
        this.requestedParamsJSON = params;
        this.isJSON = true;
        mResponseHandler = handler;
        mType = RUNNABLE;
        mRunnableObj = r;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        // post request 보냄
        BackgroundWorker backgroundWorker = new BackgroundWorker();
        String str = null;

        if (!isJSON) str = backgroundWorker.postRequestHandler(url, requestedParams);
        else  str = backgroundWorker.postRequestHandler(url, requestedParamsJSON);

        Log.i("PHP Response", str);
        return str;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        switch (mType) {
            case JSON:
                mType = -1;
                // (업데이트 후 관련된 처리 코드 없을 때)
                break;
            case HANDLER:
                mType = -1;
                Message msg = mResponseHandler.obtainMessage(id);
                mResponseHandler.sendMessage(msg);
                break;
            case RUNNABLE:
                mType = -1;
                mResponseHandler.removeCallbacks(mRunnableObj);
                mResponseHandler.postDelayed(mRunnableObj, 0);
                break;
        }
    }
}