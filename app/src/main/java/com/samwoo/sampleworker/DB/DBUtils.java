package com.samwoo.sampleworker.DB;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;

public class DBUtils {
    //DB Utils Justice Check=============================Start
    public static final int DBT_TRANSACTION_RESULT = 1;
    public static final int READY_COMPLTED = 2;

    public class QueryResultData {
        public ArrayList<HashMap<String,String>> listData;
        public Integer DBTableType;
        public int mTranSactionType;
    }

    public static class DBcreateNeed {
        public Integer DBKeyValue;
        public String DBTable;
        public String[] DBColum;
    }
    //

    public static class DBTransaction {
        public int DBTableID ;
        public int TransectionID;
        public JSONArray TranSactionJson;
        public String[] StringData;
        public String idValue;
    }

    public interface TransactionType {
        final int INSERT = 1001;
        final int READ = 1002;
        final int UPDATE = 1003;
        final int DELETE = 1004;
        final int TRANSACTION_EXIT = 1005;
    }

    public interface DBTableType {
        final int REQUEST_TABLE = 1001;
        final int AUTH_TABLE = 1002;
        final int MENTORING_TABLE = 1003;
        final int CHECKLIST_READ = 1004;
        final int CHECKLIST_INFO_READ = 1005;
        final int VIDEO_CONTROL_TABLE = 1006;
        final int DEVICE_STATE_READ = 1007;
        final int QR_CODE_READ = 1008;
    }


    public static DBcreateNeed DBCreateDataMakeFunc(int _DBKeyValue,String _DBTable,String[] _DBColum) {
        DBcreateNeed data = new DBcreateNeed();
        data.DBKeyValue = _DBKeyValue;
        data.DBTable = _DBTable;
        data.DBColum = _DBColum;
        return data;
    }

    public static DBTransaction DBTranSactionDataMakeFunc(int _DBTableID,int _TransactionID,String _IDVlaue, String[] _StringData,String _ActivityName) {
        DBTransaction data = new DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        data.idValue = _IDVlaue;
        data.StringData = _StringData;
        return data;
    }

    public static DBTransaction DBTranSactionDataMakeFunc(int _DBTableID,int _TransactionID,JSONArray _TranSactionJson,String[] _InsertData) {
        DBTransaction data = new DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        if(_TranSactionJson != null)
        {
            data.TranSactionJson = _TranSactionJson;
        }
        if(_InsertData != null)
        {
            data.StringData = _InsertData;
        }
        return data;
    }
    //===================================================================End
    private final String TAG = DBUtils.class.getSimpleName();
    public ArrayList<HashMap<String, String>> contactList = new ArrayList<>();

    private ProgressDialog pDialog;

    private Context context;
    private String readURL;
    private String insertURL;
    private String updateURL;
    private String updateURL_JSON;
    private String updateURL_test;
    private String deleteURL;
    private String[] columnName;
    private boolean isSetting;
    private boolean isUseCallBack = false;

    private Handler mRequestHandler = null;
    private int mTableIndex = 0;

    boolean isUseBlockQueue;
    public BlockingQueue<QueryResultData> mInBlockingQueue;

    public DBUtils() { }

    public DBUtils(int id, Handler handler) {
        isUseCallBack = true;
        mTableIndex = id;
        mRequestHandler = handler;
    }

    public DBUtils(int mTableID, BlockingQueue<QueryResultData> mBlockQueue) {
        mInBlockingQueue = mBlockQueue;
        isUseBlockQueue = true;
        isUseCallBack = false;
        mTableIndex = mTableID;
    }
    //Thread Read Put MessageQueueData=========================================================================
    class RequsetBasicThread extends Thread
    {
        boolean isJson;
        String url;
        JSONArray requestedParamsJSON;
        HashMap<String,String> requestedParams;
        int mType;

        RequsetBasicThread(String _url,JSONArray _RequstArray,int _Type) {
            isJson = true;
            url = _url;
            requestedParamsJSON = _RequstArray;
            mType = _Type;
        }

        RequsetBasicThread(String _url,HashMap<String,String> _RequestHashMap,int _Type) {
            isJson = false;
            url = _url;
            requestedParams = _RequestHashMap;
            mType = _Type;
        }

        @Override
        public void run() {
            //super.run();
            BackgroundWorker backgroundWorker = new BackgroundWorker();

            if (!isJson) {
                backgroundWorker.postRequestHandler(url, requestedParams);
            } else {
                backgroundWorker.postRequestHandler(url, requestedParamsJSON);
            }
            Log.i("launch : ","Update End");

            QueryResultData data = new QueryResultData();
            data.DBTableType =mTableIndex;
            data.mTranSactionType = mType;
            try {
                if (!Thread.currentThread().isInterrupted()) mInBlockingQueue.put(data);
            } catch (InterruptedException e) {
                Log.e("CustomError : ",e.getMessage());
            }
        }
    }

    Runnable ExitWorker = new Runnable() {
        @Override
        public void run() {
            QueryResultData data = new QueryResultData();
            data.mTranSactionType = TransactionType.TRANSACTION_EXIT;
            try {
                if (!Thread.currentThread().isInterrupted()) mInBlockingQueue.put(data);
            } catch (InterruptedException e) {
                Log.e("CustomError : ",e.getMessage());
            }
        }
    };

    Runnable queryWorker = new Runnable() {
        @Override
        public void run() {
            JsonParser sh = new JsonParser();

            // Making a request to url and getting response
            String jsonStr = sh.convertJson(readURL);

            QueryResultData data = new QueryResultData(); //+added by swseo

            ArrayList<HashMap<String,String>> inContactList = new ArrayList<>();
            inContactList.clear();

            //Log.i(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray jsonArray = jsonObj.getJSONArray("result");

//                    contactList.clear();

                    // looping through All Contacts
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        String[] output = new String[columnName.length];

                        String id = c.getString("id");
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            output[j] = c.getString(columnName[j]);
                        }

                        // tmp hash map for single contact
                        HashMap<String, String> map = new HashMap<>();

                        // adding each child node to HashMap key => value
                        map.put("id", id);
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            map.put(columnName[j], output[j]);
                        }

                        // adding contact to contact list
                        inContactList.add(map);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Couldn't get json from server. Check LogCat for possible errors!");
            }

            //QueryResultData data = new QueryResultData();
            data.listData = inContactList;
            data.DBTableType =mTableIndex;
            data.mTranSactionType = TransactionType.READ;
            contactList = inContactList;

            try {
                //쿼리 실패시 데이터 처리 확인
                if (!Thread.currentThread().isInterrupted()) mInBlockingQueue.put(data);
            } catch (InterruptedException e) {
                Log.e("CustomError : ",e.getMessage());
            }
        }
    };

    /**
     * DB 연결을 위한 함수
     * 다른 함수를 쓰기위해서 설정 필요
     * @param constantTableName DB 테이블 이름 (Constant 클래스에 입력)
     * @param columnName DB 테이블의 칼럼 이름 (id는 제외)
     */
    public void setDB(Context context, String constantTableName, String[] columnName) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_JSON = Constant.PATH + constantTableName + "update_json.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";
        this.context = context;

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);

        isSetting = true;

        //read();
    }

    public void setDB(Context context, String constantTableName, String[] columnName, boolean _noRead) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_JSON = Constant.PATH + constantTableName + "update_json.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";
        this.context = context;

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);

        isSetting = true;

        //read();
    }


    public void setDB(String constantTableName, String[] columnName) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);
        isSetting = true;
    }

    public ArrayList<HashMap<String, String>> getContactList() {
        return contactList;
    }

    public String getID(String columnName, String value) {
        String id = null;

        for (int i = 0; i < getContactList().size(); i++) {
            String test = getContactList().get(i).get(columnName);

            assert test != null;
            if (test.equals(value)) {
                id = getContactList().get(i).get("id");
                break;
            }
        }

        return id;
    }

    public int getIndexNumber(String id) {
        int indexNumber = -1;

        for (int i = 0; i < getContactList().size(); i++) {
            if (Objects.requireNonNull(getContactList().get(i).get("id")).equals(id)) {
                indexNumber = i;
                break;
            }
        }

        return indexNumber;
    }

    public void insert(String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(insertURL, requestedParams);
            postRequestHandler.execute();
        }
    }
    //add Merge==================================================================================++
    public void insert(boolean service, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            RequsetBasicThread PostRequstThread = new RequsetBasicThread(insertURL, requestedParams, TransactionType.INSERT);
            PostRequstThread.start();
        }
    }
    //add Merge==================================================================================--
    public void update(String id, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL, requestedParams);
            postRequestHandler.execute();
        }
    }

    public void update(JSONArray objList, Handler handler, Runnable r) {
        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL_test, objList, handler, r);
            postRequestHandler.execute();
        }
    }

    public void update(String id, String[] input, Handler handler, int type) {
        JSONArray objList = new JSONArray();
        JSONObject obj = new JSONObject();

        try {
            obj.put("id", id);

            for (int i = 0; i < columnName.length; i++) {
                if (columnName[i] == null) break;
                obj.put(columnName[i], input[i]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        objList.put(obj);

        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL_JSON, objList, handler, type);
            postRequestHandler.execute();
        }
    }

    public void update(String id, String[] input, Handler handler, Runnable r) {
        JSONArray objList = new JSONArray();
        JSONObject obj = new JSONObject();

        try {
            obj.put("id", id);

            for (int i = 0; i < columnName.length; i++) {
                if (columnName[i] == null) break;
                obj.put(columnName[i], input[i]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        objList.put(obj);

        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL_JSON, objList, handler, r);
            postRequestHandler.execute();
        }
    }
    //add Merge==================================================================================++
    public void update(boolean _Service, JSONArray objList) {
        RequsetBasicThread PostRequstThread = new RequsetBasicThread(updateURL_test, objList, TransactionType.UPDATE);
        PostRequstThread.start();
    }
    //add Merge==================================================================================--
    //add Merge==================================================================================++
    public void update(boolean _Service, String id, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if(requestedParams.size() != 0) {
            RequsetBasicThread PostRequstThread = new RequsetBasicThread(updateURL, requestedParams, TransactionType.UPDATE);
            PostRequstThread.start();
        }
    }
    //add Merge==================================================================================--


    public void delete(String columnName, String input) {
        HashMap<String, String> requestedParams = new HashMap<>();
        String id = getID(columnName, input);

        if (id != null) requestedParams.put("id", id);

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(deleteURL, requestedParams);
            postRequestHandler.execute();
        }
    }

    public void delete(String id) {
        HashMap<String, String> requestedParams = new HashMap<>();

        if (id != null) requestedParams.put("id", id);

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(deleteURL, requestedParams);
            postRequestHandler.execute();
        }
    }
    //add Merge==================================================================================++
    public void delete(boolean _Service,JSONArray _objList) {
        if (_objList.length() != 0) {
            RequsetBasicThread PostRequstThread = new RequsetBasicThread(deleteURL, _objList, TransactionType.DELETE);
            PostRequstThread.start();
        }
    }
    //add Merge==================================================================================--
    //add Merge==================================================================================++
    public void Exit(boolean _Service) {
        Thread ExitDataPutwoker = new Thread(ExitWorker);
        ExitDataPutwoker.start();
    }
    //add Merge==================================================================================--

    public void read() { new Read().execute(); }

    public void read(int id, Handler handler) {
        isUseCallBack = true;
        mTableIndex = id;
        mRequestHandler = handler;

        new Read().execute();
    }

    public void read(boolean use) {
        Thread t = new Thread(queryWorker);
        t.start();
    }

    @SuppressLint("StaticFieldLeak")
    private class Read extends AsyncTask<Void, Void, Void> {

        //private ListAdapter adapter;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            if (context != null && isSetting) {
                isSetting = false;

                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            JsonParser sh = new JsonParser();

            // Making a request to url and getting response
            String jsonStr = sh.convertJson(readURL);

            ArrayList<HashMap<String,String>> inConTactList = new ArrayList<>();
            inConTactList.clear();

            Log.i(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray jsonArray = jsonObj.getJSONArray("result");

//                    contactList.clear();

                    // looping through All Contacts
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        String[] output = new String[columnName.length];

                        String id = c.getString("id");
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            output[j] = c.getString(columnName[j]);
                        }

                        // tmp hash map for single contact
                        HashMap<String, String> map = new HashMap<>();

                        // adding each child node to HashMap key => value
                        map.put("id", id);
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            map.put(columnName[j], output[j]);
                        }

                        // adding contact to contact list
                        inConTactList.add(map);
                        contactList = inConTactList;
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Couldn't get json from server. Check LogCat for possible errors!");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog != null) {
                if (pDialog.isShowing()) pDialog.dismiss();
            }

            Message msg;

            if(isUseCallBack) {
                switch(mTableIndex) {
                    case DBTableType.REQUEST_TABLE:
                    case DBTableType.AUTH_TABLE:
                    case DBTableType.MENTORING_TABLE:
                    case DBTableType.VIDEO_CONTROL_TABLE:
                        msg = mRequestHandler.obtainMessage(mTableIndex);
                        mRequestHandler.sendMessage(msg);
                        break;
                    case DBTableType.CHECKLIST_READ:
                    case DBTableType.CHECKLIST_INFO_READ:
                    case DBTableType.DEVICE_STATE_READ:
                        isUseCallBack = false;
                        msg = mRequestHandler.obtainMessage(mTableIndex);
                        mRequestHandler.sendMessage(msg);
                        break;
                }
            }
        }
    }
}