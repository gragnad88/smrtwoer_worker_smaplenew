package com.samwoo.sampleworker.DB;

public class Constant {
    //PHP 파일 주소 목록 설정

//    public static final String IP = "http://13.209.102.191"; // 해양 조사원
//    public static final String IP = "http://15.164.172.188"; // KLCSM
    public static final String IP = "http://13.209.203.108"; // 개발용
    public static final String PATH = IP + "/work/";
    //region Table 이름
    public static final String AUTH = "auth_test/";
    public static final String CHECKLIST = "checklist/";
    public static final String CHECKLIST_INFO = "checklist_info/";
    public static final String DEVICE_STATE = "device_state/";
    public static final String QR_CODE = "qr_code/";
    public static final String TRANSMISSION = "transmission/";
    public static final String VIDEO_CONTROL = "video_control/";
    //endregion

    public static final String FILE_PATH = IP + "/uploads/";
    public static final String FILE_PATH_PHP = IP + "/uploadFile.php";

    public static final String GET_METHOD = "GET";
    public static final String POST_METHOD = "POST";
}
