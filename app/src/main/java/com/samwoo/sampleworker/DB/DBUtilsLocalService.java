package com.samwoo.sampleworker.DB;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;

public class DBUtilsLocalService {
    public static final int DBT_TRANSACTION_RESULT = 1;
    public static final int READY_COMPLETED = 2;

    private final String TAG = DBUtilsLocalService.class.getSimpleName();
    //public ArrayList<HashMap<String, String>> contactList = new ArrayList<>(); //+commented by swseo

    private ProgressDialog pDialog;

    private Context context;
    private String readURL;
    private String insertURL;
    private String updateURL;
    private String updateURL_test;
    private String deleteURL;
    private String deleteURL_json;
    private String[] columnName;
    private boolean isSetting;
    private boolean isUseCallBack = false;

    public class QueryResultData {
        public ArrayList<HashMap<String,String>> listData;
        public Integer DBTableType;
        public int mTranSactionType;
        public int arg;
    }

    public static class DBcreateNeed {
        public Integer DBKeyValue;
        public String DBTable;
        public String[] DBColum;
    }

    public static class DBTransaction {
        public int DBTableID ;
        public int TransectionID;
        public JSONArray TranSactionJson;
        public String[] StringData;
        public String idValue;
        public int mArg;
    }

    public interface TransactionType {
        final int INSERT = 1001;
        final int READ = 1002;
        final int UPDATE = 1003;
        final int DELETE = 1004;
        final int TRANSACTION_EXIT = 1005;
    }

    public interface DBTableType {
        final int REQUEST_TABLE = 1001;
        final int AUTH_TABLE = 1002;
        final int CHECKLIST_READ = 1003;
        final int CHECKLIST_INFO_READ = 1004;
        final int CHECKLIST_UPDATE = 1005;
        final int CHECKLIST_INFO_DELETE = 1006;
        final int CHECKLIST_DELETE = 1007;
        final int CHECKLIST_INSERT = 1008;
        final int MONITORING_AUTH_TABLE = 1009;
        final int MONITORING_STATE_TABLE = 1010;
        final int MENTORING_READ = 1011;
        final int DEVICE_STATE = 1012;
        final int REQUEST_READ = 1013;
        final int VIDEO_CONTROL_UPDATE = 1014;
        final int QR_CODE_TABLE = 1015;
    }

    public static DBUtilsLocalService.DBcreateNeed DBCreateDataMakeFunc(int _DBKeyValue, String _DBTable, String[] _DBColum) {
        DBUtilsLocalService.DBcreateNeed data = new DBUtilsLocalService.DBcreateNeed();
        data.DBKeyValue = _DBKeyValue;
        data.DBTable = _DBTable;
        data.DBColum = _DBColum;
        return data;
    }

    public static DBUtilsLocalService.DBTransaction DBTranSactionDataMakeFunc(int _DBTableID, int _TransactionID, String _IDVlaue, String[] _StringData, String _ActivityName) {
        DBUtilsLocalService.DBTransaction data = new DBUtilsLocalService.DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        data.idValue = _IDVlaue;
        data.StringData = _StringData;
        return data;
    }

    public static DBUtilsLocalService.DBTransaction DBTranSactionDataMakeFunc(int _DBTableID, int _TransactionID, String _IDVlaue, String[] _StringData, int mArg) {
        DBUtilsLocalService.DBTransaction data = new DBUtilsLocalService.DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        data.idValue = _IDVlaue;
        data.StringData = _StringData;
        data.mArg = mArg;
        return data;
    }

    public static DBUtilsLocalService.DBTransaction DBTranSactionDataMakeFunc(int _DBTableID, int _TransactionID, JSONArray _TranSactionJson, String[] _InsertData) {
        DBUtilsLocalService.DBTransaction data = new DBUtilsLocalService.DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        if(_TranSactionJson != null) data.TranSactionJson = _TranSactionJson;
        if(_InsertData != null) data.StringData = _InsertData;
        return data;
    }

    private Handler mRequestHandler = null;
    private Handler mHandler;
    private int mTableIndex = 0;
    boolean isUseBlockQueue;

    public BlockingQueue<DBUtilsLocalService.QueryResultData> mInBlockingQueue;

    public DBUtilsLocalService() { }

    public DBUtilsLocalService(int mTableID, BlockingQueue<DBUtilsLocalService.QueryResultData> mBlockQueue) {
        mInBlockingQueue = mBlockQueue;
        isUseBlockQueue = true;
        isUseCallBack = false;
        mTableIndex = mTableID;
    }

    public DBUtilsLocalService(int id, Handler handler) {
        isUseCallBack = true;
        mTableIndex = id;
        mRequestHandler = handler;
    }

    public DBUtilsLocalService(int id, Handler handler, boolean use) {
        isUseCallBack = true;
        mTableIndex = id;
        mRequestHandler = handler;

        //mHandler = new Handler();
    }

    //Thread Read Put MessageQueueData=========================================================================
    class RequestBasicThread extends Thread {
        boolean isJson;
        String url;
        JSONArray requestedParamsJSON;
        HashMap<String,String> requestedParams;
        int mType;
            int arg1 = -1;
        RequestBasicThread(String _url, JSONArray _RequstArray, int _Type) {
            isJson = true;
            url = _url;
            requestedParamsJSON = _RequstArray;
            mType = _Type;
        }

        RequestBasicThread(String _url, JSONArray _RequstArray, int _Type, int mArg1) {
            isJson = true;
            url = _url;
            requestedParamsJSON = _RequstArray;
            mType = _Type;
            arg1 = mArg1;
        }

        RequestBasicThread(String _url, HashMap<String, String> _RequestHashMap, int _Type) {
            isJson = false;
            url = _url;
            requestedParams = _RequestHashMap;
            mType = _Type;
        }

        RequestBasicThread(String _url, HashMap<String, String> _RequestHashMap, int _Type, int mArg1) {
            isJson = false;
            url = _url;
            requestedParams = _RequestHashMap;
            mType = _Type;
            arg1 = mArg1;
        }

        @Override
        public void run() {
            //super.run();
            BackgroundWorker backgroundWorker = new BackgroundWorker();

            if (!isJson)  backgroundWorker.postRequestHandler(url, requestedParams);
            else backgroundWorker.postRequestHandler(url, requestedParamsJSON);

            Log.i("launch : ","Update End");

            DBUtilsLocalService.QueryResultData data = new DBUtilsLocalService.QueryResultData();
            data.DBTableType =mTableIndex;
            data.mTranSactionType = mType;

            if(arg1 != -1) data.arg = arg1;

            try {
                if (!Thread.currentThread().isInterrupted()) mInBlockingQueue.put(data);
            } catch (InterruptedException e) {
                Log.e("CustomError : ", e.getMessage());
            }
        }
    }

    Runnable ExitWorker = new Runnable() {
        @Override
        public void run() {
            DBUtilsLocalService.QueryResultData data = new DBUtilsLocalService.QueryResultData();
            data.mTranSactionType = DBUtils.TransactionType.TRANSACTION_EXIT;
            try {
                if (!Thread.currentThread().isInterrupted()) mInBlockingQueue.put(data);
            } catch (InterruptedException e) {
                Log.e("CustomError : ", e.getMessage());
            }
        }
    };

    Runnable queryWorker = new Runnable() {
        @Override
        public void run() {
            JsonParser sh = new JsonParser();

            // Making a request to url and getting response
            String jsonStr = sh.convertJson(readURL);

            QueryResultData data = new QueryResultData(); //+added by swseo
            data.listData = new ArrayList<>(); //+added by swseo
            data.listData.clear(); //+added by swseo

            //Log.i(TAG, "Response from url: " + jsonStr);
            //ArrayList<HashMap<String, String>> inContactList = new ArrayList<>(); //+commented by swseo
            //inContactList.clear(); //+commented by swseo

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray jsonArray = jsonObj.getJSONArray("result");

                    // looping through All Contacts
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        String[] output = new String[columnName.length];

                        String id = c.getString("id");
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            output[j] = c.getString(columnName[j]);
                        }

                        // tmp hash map for single contact
                        HashMap<String, String> map = new HashMap<>();

                        // adding each child node to HashMap key => value
                        map.put("id", id);
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            map.put(columnName[j], output[j]);
                        }

                        // adding contact to contact list
                        //inContactList.add(map); //+commented by swseo
                        data.listData.add(map); //+added by swseo
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Couldn't get json from server. Check LogCat for possible errors!");
            }

            //DBUtilsLocalService.QueryResultData data = new DBUtilsLocalService.QueryResultData(); //+commented by swseo
            //data.listData = inContactList; //+commented by swseo
            data.DBTableType =mTableIndex;
            data.mTranSactionType = DBUtils.TransactionType.READ;
            //contactList = inContactList; //+commented by swseo

            try {
                //쿼리 실패시 데이터 처리 확인
                if (!Thread.currentThread().isInterrupted()) mInBlockingQueue.put(data);
            } catch (InterruptedException e) {
                Log.e("CustomError : ",e.getMessage());
            }
        }
    };

    /**
     * DB 연결을 위한 함수
     * 다른 함수를 쓰기위해서 설정 필요
     * @param constantTableName DB 테이블 이름 (Constant 클래스에 입력)
     * @param columnName DB 테이블의 칼럼 이름 (id는 제외)
     **/
    //+commented by swseo
    /*
    public void setDB(Context context, String constantTableName, String[] columnName) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";
        this.deleteURL_json = Constant.PATH + constantTableName + "delete_json.php";
        this.context = context;

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);

        isSetting = true;

        read();
    }
    */
    //-commented by swseo

    public void setDB(Context context, String constantTableName, String[] columnName, boolean use) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";
        this.deleteURL_json = Constant.PATH + constantTableName + "delete_json.php";
        this.context = context;

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);

        isSetting = false;
    }


    public void setDB(String constantTableName, String[] columnName) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";
        this.deleteURL_json = Constant.PATH + constantTableName + "delete_json.php";

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);

        isSetting = true;

        //read(); //+commented by swseo
    }

    //+commented by swseo
    /*
    public ArrayList<HashMap<String, String>> getContactList() {
        return contactList;
    }

    public String getID(String columnName, String value) {
        String id = null;

        for (int i = 0; i < getContactList().size(); i++) {
            String test = getContactList().get(i).get(columnName);

            assert test != null;
            if (test.equals(value)) {
                id = getContactList().get(i).get("id");
                break;
            }
        }

        return id;
    }

    public int getIndexNumber(String id) {
        int indexNumber = -1;

        for (int i = 0; i < getContactList().size(); i++) {
            if (Objects.requireNonNull(getContactList().get(i).get("id")).equals(id)) {
                indexNumber = i;
                break;
            }
        }

        return indexNumber;
    }
    */
    //-commented by swseo


    public static int getIndexNumber(String id, ArrayList<HashMap<String,String>> contactList) {
        int indexNumber = -1;

        for (int i = 0; i < contactList.size(); i++) {
            if (Objects.requireNonNull(contactList.get(i).get("id")).equals(id)) {
                indexNumber = i;
                break;
            }
        }

        return indexNumber;
    }

    public void insert(String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(insertURL, requestedParams);
            postRequestHandler.execute();
        }
    }



    public void insert(boolean service, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            DBUtilsLocalService.RequestBasicThread PostRequstThread = new DBUtilsLocalService.RequestBasicThread(insertURL, requestedParams, DBUtils.TransactionType.INSERT);
            PostRequstThread.start();
        }
    }

    public void update(JSONArray objList) {
        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL_test, objList);
            postRequestHandler.execute();
        }
    }


    public void update(JSONArray objList, Handler handler, Runnable r) {
        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL_test, objList, handler, r);
            postRequestHandler.execute();
        }
    }

    public void update(String id, String[] input, float use) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL, requestedParams);
            postRequestHandler.execute();
        }
    }

    public void update(String id, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(updateURL, requestedParams);
            postRequestHandler.execute();
        }
    }

    public void update(boolean _Service, JSONArray objList) {
        DBUtilsLocalService.RequestBasicThread PostRequstThread = new DBUtilsLocalService.RequestBasicThread(updateURL_test, objList, DBUtils.TransactionType.UPDATE);
        PostRequstThread.start();
    }

    public void update(boolean _Service, String id, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if(requestedParams.size() != 0) {
            DBUtilsLocalService.RequestBasicThread PostRequstThread = new DBUtilsLocalService.RequestBasicThread(updateURL, requestedParams, DBUtils.TransactionType.UPDATE);
            PostRequstThread.start();
        }
    }

    public void update(boolean _Service, String id, String[] input, int arg1) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if(requestedParams.size() != 0) {
            DBUtilsLocalService.RequestBasicThread PostRequstThread = new DBUtilsLocalService.RequestBasicThread(updateURL, requestedParams, DBUtils.TransactionType.UPDATE,arg1);
            PostRequstThread.start();
        }
    }

    //+commented by swseo
    /*
    public void delete(String columnName, String input, Handler handler, int type) {
        JSONArray objList = new JSONArray();
        JSONObject obj = new JSONObject();

        // delete를 호출하는 곳에서 getID 구현
        String id = getID(columnName, input);
        try {
            obj.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        objList.put(obj);

        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(deleteURL_json, objList, handler, type);
            postRequestHandler.execute();
        }
    }
    */
    //-commented by swseo

    public void delete(String id, String columnName, String input, Handler handler, int type) {
        JSONArray objList = new JSONArray();
        JSONObject obj = new JSONObject();

        try {
            obj.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        objList.put(obj);

        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(deleteURL_json, objList, handler, type);
            postRequestHandler.execute();
        }
    }

    public void delete(String id) {
        HashMap<String, String> requestedParams = new HashMap<>();

        if (id != null) requestedParams.put("id", id);

        if (requestedParams.size() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(deleteURL, requestedParams);
            postRequestHandler.execute();
        }
    }

    public void delete(JSONArray objList, Handler handler, int type) {
        if (objList.length() != 0) {
            PostRequestHandler postRequestHandler = new PostRequestHandler(deleteURL_json, objList, handler, type);
            postRequestHandler.execute();
        }
    }

    public void delete(boolean _Service, JSONArray _objList) {
        if (_objList.length() != 0) {
            DBUtilsLocalService.RequestBasicThread PostRequstThread = new DBUtilsLocalService.RequestBasicThread(updateURL_test, _objList, DBUtils.TransactionType.DELETE);
            PostRequstThread.start();

        }
    }

    public void Exit(boolean _Service) {
        Thread ExitDataPutwoker = new Thread(ExitWorker);
        ExitDataPutwoker.start();
    }

    /*
    public void read() {
        new DBUtilsLocalService.Read().execute();
    }
    */

    public void read(boolean use) {
        Thread t = new Thread(queryWorker);
        t.start();
    }
}
