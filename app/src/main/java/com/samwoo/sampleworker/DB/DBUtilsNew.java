package com.samwoo.sampleworker.DB;

import android.util.Log;

import com.samwoo.sampleworker.FragmentFolder.INF_DBTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DBUtilsNew implements INF_DBTransaction<ArrayList<HashMap<String,String>>> {

    public DBUtilsNew()
    {    }

    public DBUtilsNew(int mTableID)
    {
        mTableIndex = mTableID;
    }

    private INF_DBTransaction mParentInterface;
    private String readURL;
    private String insertURL;
    private String updateURL;
    private String updateURL_JSON;
    private String updateURL_test;
    private String deleteURL;
    private String[] columnName;
    private int mTableIndex = 0;
    public int mTableType = 0;
    private QueryResultData mResultData;
    private int nowTranSactionProcess = 0;

    //
    public void setmParentInterface(INF_DBTransaction mParentInterface) {
        this.mParentInterface = mParentInterface;
    }

    public int getNowTranSactionProcess() {
        return nowTranSactionProcess;
    }

    public QueryResultData getmResultData() {
        return mResultData;
    }

    //interface
    @Override
    public void Request(DBTransaction mTranSaction) {
        switch (mTranSaction.TransectionID)
        {
            case DBUtilsLocalService.TransactionType.INSERT:
                String[] inputData = mTranSaction.StringData;
                insert(inputData);
                break;
            case DBUtilsLocalService.TransactionType.READ:
                read();
                break;
            case DBUtilsLocalService.TransactionType.UPDATE:
                if (mTranSaction.StringData != null) {
                    String[] StringData = mTranSaction.StringData;
                    String IDdata = mTranSaction.idValue;

                    if(mTranSaction.mArg != 0) {
                        update(IDdata, StringData, mTranSaction.mArg);
                    } else if(mTranSaction.mArg == 0) {
                        update(IDdata, StringData);
                    }
                } else if (mTranSaction.TranSactionJson != null) {
                    JSONArray getObjList = mTranSaction.TranSactionJson;
                    update(getObjList);
                }
                break;
            case DBUtilsLocalService.TransactionType.DELETE:
                JSONArray DeleteJson = mTranSaction.TranSactionJson;
                delete(DeleteJson);
                break;
        }
    }

    @Override
    public void Request(DBTransaction[] data) {
        //ignore DBRead Utils
    }

    @Override
    public void ResponseConTactData(ArrayList<HashMap<String, String>> data, int TableID) {
        //ignore
    }

    public class QueryResultData {
        public ArrayList<HashMap<String,String>> listData;
        public Integer DBTableType;
        public int mTranSactionType;
    }

    public static class DBcreateNeed {
        public Integer DBKeyValue;
        public String DBTable;
        public String[] DBColum;
    }
    //

    public static class DBTransaction {
        public int DBTableID ;
        public int TransectionID;
        public JSONArray TranSactionJson;
        public String[] StringData;
        public String idValue;
        public int mArg;
    }

    public interface TransactionType {
        final int INSERT = 1001;
        final int READ = 1002;
        final int UPDATE = 1003;
        final int DELETE = 1004;
        final int TRANSACTION_EXIT = 1005;
        final int INSERT_COMPLTE = 1006;
        final int READ_COMPLETE = 1007;
        final int UPDATE_COMPLETE = 1008;
        final int DELETE_COMPLETE = 1009;
    }

    public interface DBTableType {
        final int REQUEST_TABLE = 2001;
        final int AUTH_TABLE = 2002;
        final int MENTORING_TABLE = 2003;
        final int CHECKLIST_READ = 2004;
        final int CHECKLIST_INFO_READ = 2005;
        final int VIDEO_CONTROL_TABLE = 2006;
        final int DEVICE_STATE_READ = 2007;
        final int QR_CODE_READ = 2008;
    }

    public static DBUtilsNew.DBcreateNeed DBCreateDataMakeFunc(int _DBKeyValue, String _DBTable, String[] _DBColum) {
        DBUtilsNew.DBcreateNeed data = new DBUtilsNew.DBcreateNeed();
        data.DBKeyValue = _DBKeyValue;
        data.DBTable = _DBTable;
        data.DBColum = _DBColum;
        return data;
    }

    public static DBTransaction DBTranSactionDataMakeFunc(int _DBTableID, int _TransactionID, String _IDVlaue, String[] _StringData, String _ActivityName) {
        DBTransaction data = new DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        data.idValue = _IDVlaue;
        data.StringData = _StringData;
        return data;
    }

    public static DBTransaction DBTranSactionDataMakeFunc(int _DBTableID, int _TransactionID, JSONArray _TranSactionJson, String[] _InsertData) {
        DBTransaction data = new DBTransaction();
        data.DBTableID = _DBTableID;
        data.TransectionID =_TransactionID;
        if(_TranSactionJson != null)
        {
            data.TranSactionJson = _TranSactionJson;
        }
        if(_InsertData != null)
        {
            data.StringData = _InsertData;
        }
        return data;
    }

    public void setDB(String constantTableName, String[] columnName) {
        this.readURL = Constant.PATH + constantTableName + "getAll.php";
        this.insertURL = Constant.PATH + constantTableName + "insert.php";
        this.updateURL = Constant.PATH + constantTableName + "update.php";
        this.updateURL_test = Constant.PATH + constantTableName + "update_test.php";
        this.deleteURL = Constant.PATH + constantTableName + "delete.php";

        this.columnName = new String[columnName.length];
        System.arraycopy(columnName, 0, this.columnName, 0, columnName.length);
    }

    public void read() {
        Thread t = new Thread(queryWorker);
        t.start();
    }

    public void delete(JSONArray _objList) {
        if (_objList.length() != 0) {
            RequestBasicThread PostRequstThread = new RequestBasicThread(updateURL_test, _objList, DBUtils.TransactionType.DELETE);
            PostRequstThread.start();
        }
    }

    public void update(JSONArray objList) {
        RequestBasicThread PostRequstThread = new RequestBasicThread(updateURL_test, objList, DBUtils.TransactionType.UPDATE);
        PostRequstThread.start();
    }

    public void update(String id, String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if(requestedParams.size() != 0) {
            RequestBasicThread PostRequstThread = new RequestBasicThread(updateURL, requestedParams, TransactionType.UPDATE);
            PostRequstThread.start();
        }
    }

    public void update(String id, String[] input, int arg1) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        requestedParams.put("id", id);
        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if(requestedParams.size() != 0) {
            RequestBasicThread PostRequstThread = new RequestBasicThread(updateURL, requestedParams, DBUtils.TransactionType.UPDATE,arg1);
            PostRequstThread.start();
        }
    }

    public void insert(String[] input) {
        HashMap<String, String> requestedParams = new HashMap<>();

        String[] column = new String[columnName.length];
        System.arraycopy(input, 0, column, 0, columnName.length);

        for (int i = 0; i < columnName.length; i++) {
            if (columnName[i] == null) break;
            requestedParams.put(columnName[i], column[i]);
        }

        if (requestedParams.size() != 0) {
            RequestBasicThread PostRequstThread = new RequestBasicThread(insertURL, requestedParams, TransactionType.INSERT);
            PostRequstThread.start();
        }
    }

    class RequestBasicThread extends Thread
    {
        boolean isJson;
        String url;
        JSONArray requestedParamsJSON;
        HashMap<String,String> requestedParams;
        int mType;
        int arg1 = -1;
        RequestBasicThread(String _url,JSONArray _RequstArray,int _Type) {
            isJson = true;
            url = _url;
            requestedParamsJSON = _RequstArray;
            mType = _Type;
        }
        RequestBasicThread(String _url, JSONArray _RequstArray, int _Type, int mArg1) {
            isJson = true;
            url = _url;
            requestedParamsJSON = _RequstArray;
            mType = _Type;
            arg1 = mArg1;
        }

        RequestBasicThread(String _url, HashMap<String, String> _RequestHashMap, int _Type) {
            isJson = false;
            url = _url;
            requestedParams = _RequestHashMap;
            mType = _Type;
        }

        RequestBasicThread(String _url, HashMap<String, String> _RequestHashMap, int _Type, int mArg1) {
            isJson = false;
            url = _url;
            requestedParams = _RequestHashMap;
            mType = _Type;
            arg1 = mArg1;
        }
        @Override
        public void run() {
            //super.run();
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            //Check Transaction Process
            checkTransactionProcess(true,mType);

            if (!isJson) {
                backgroundWorker.postRequestHandler(url, requestedParams);
            } else {
                backgroundWorker.postRequestHandler(url, requestedParamsJSON);
            }
            Log.i("launch : ","Update End");

            QueryResultData data = new QueryResultData();
            data.DBTableType =mTableIndex;
            data.mTranSactionType = mType;

            try {
                 mResultData = data;
                //Check Transaction Process
                checkTransactionProcess(false,mType);
            } catch (Exception e) {
                Log.e("CustomError : ",e.getMessage());
            }
        }
    }

    //
    void checkTransactionProcess(boolean isStart,int mTranSactionType)
    {
        switch (mTranSactionType)
        {
            case TransactionType.INSERT:
                if(!isStart)
                {
                    nowTranSactionProcess = TransactionType.INSERT;
                }
                else if(isStart)
                {
                    nowTranSactionProcess = TransactionType.INSERT_COMPLTE;
                    //complete data
                    mParentInterface.ResponseConTactData(mResultData,mTableType);
                }
              break;
            case TransactionType.DELETE:
                if(!isStart)
                {
                    nowTranSactionProcess = TransactionType.DELETE;
                }
                else if(isStart)
                {
                    nowTranSactionProcess = TransactionType.DELETE_COMPLETE;
                    //complete data
                    mParentInterface.ResponseConTactData(mResultData,mTableType);
                }
                break;
            case  TransactionType.UPDATE:
                if(!isStart)
                {
                    nowTranSactionProcess = TransactionType.UPDATE;
                }
                else if(isStart)
                {
                    nowTranSactionProcess = TransactionType.UPDATE_COMPLETE;
                    //complete data
                    mParentInterface.ResponseConTactData(mResultData,mTableType);
                }
                break;

            case  TransactionType.READ:
                if(!isStart)
                {
                    nowTranSactionProcess = TransactionType.READ;
                }
                else if(isStart)
                {
                    nowTranSactionProcess = TransactionType.READ_COMPLETE;
                    //complete data
                    mParentInterface.ResponseConTactData(mResultData,mTableType);
                }
                break;
        }

    }


    Runnable queryWorker = new Runnable() {
        @Override
        public void run() {
            JsonParser sh = new JsonParser();
            checkTransactionProcess(true,TransactionType.READ);
            String jsonStr = sh.convertJson(readURL);

            QueryResultData data = new QueryResultData(); //+added by swseo

            ArrayList<HashMap<String,String>> inContactList = new ArrayList<>();
            inContactList.clear();

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray jsonArray = jsonObj.getJSONArray("result");
                    // looping through All Contacts
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        String[] output = new String[columnName.length];

                        String id = c.getString("id");
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            output[j] = c.getString(columnName[j]);
                        }

                        // tmp hash map for single contact
                        HashMap<String, String> map = new HashMap<>();

                        // adding each child node to HashMap key => value
                        map.put("id", id);
                        for (int j = 0; j < columnName.length; j++) {
                            if (columnName[j] == null || columnName[j].equals("")) break;
                            map.put(columnName[j], output[j]);
                        }

                        // adding contact to contact list
                        inContactList.add(map);
                    }
                } catch (final JSONException e) {
                    //Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            } else {
                //Log.e(TAG, "Couldn't get json from server. Check LogCat for possible errors!");
            }

            //QueryResultData data = new QueryResultData();
            data.listData = inContactList;
            data.DBTableType =mTableIndex;
            data.mTranSactionType = DBUtils.TransactionType.READ;
            try {
                //쿼리 실패시 데이터 처리 확인
                mResultData = data;
                //
                checkTransactionProcess(false,TransactionType.READ);
            } catch (Exception e) {
                Log.e("CustomError : ",e.getMessage());
            }
        }
    };
}
