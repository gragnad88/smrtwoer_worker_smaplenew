package com.samwoo.sampleworker;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;

import java.util.Objects;

public class ReceptionVideoPlayerActivity extends AppCompatActivity {
    SmartWorkApplication mApp;

    private static final int READ_FINISH = 7000;

    public PlayerView videoView;
    public SimpleExoPlayer player = null;
    public float currentPosition = 0.0f;
    boolean isPlay;

//    BackgroundThread thread = new BackgroundThread();

    Handler mHandler = null;

    DBUtils videoTransmission = null/*new DBUtils()*/;

    ExtractorMediaSource mediaSource;
    DefaultDataSourceFactory dataSourceFactory;

    private final Runnable BackgroundRunnable = new Runnable() {
        @Override
        public void run() {
            videoTransmission.read();
        }
    } ;

    protected Handler createReadBackGroundThreadHandler() {
        return new ReadBackgroundThreadWorkerHandler(getMainLooper());
    }

    protected class ReadBackgroundThreadWorkerHandler extends Handler {
        public ReadBackgroundThreadWorkerHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DBUtils.DBTableType.VIDEO_CONTROL_TABLE:
                    if (player != null) {
                        currentPosition = Float.parseFloat(Objects.requireNonNull(videoTransmission.getContactList().get(0).get("seek")));

                        if (Objects.requireNonNull(videoTransmission.getContactList().get(0).get("play")).equals("true")) isPlay = true;
                        else if (Objects.requireNonNull(videoTransmission.getContactList().get(0).get("play")).equals("false")) isPlay = false;

                        if(!isPlay) player.seekTo((long)currentPosition);

                        player.setPlayWhenReady(isPlay);

                        Thread t = new Thread(BackgroundRunnable);
                        t.start();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        player.release();
        player = null;
        mHandler = null;
    }

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_reception_video_player);

        mApp = (SmartWorkApplication)getApplication();

        mHandler = createReadBackGroundThreadHandler();
        videoTransmission = new DBUtils(DBUtils.DBTableType.VIDEO_CONTROL_TABLE, mHandler);
        videoTransmission.setDB(this, Constant.VIDEO_CONTROL, new String[]{ "play", "seek" });
        videoTransmission.read();

        videoView = findViewById(R.id.videoView);

        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector());

        if (mApp.isControlVideo()) videoView.setPlayer(player);
        else {
            videoView.setUseController(false);
            videoView.setPlayer(player);

            Thread t = new Thread(BackgroundRunnable);
            t.start();
//            if (!thread.isAlive()) {
//                thread.setDaemon(true);
//                thread.start();
//            }
        }

        dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "SampleWorker"));
        mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(Environment.getExternalStorageDirectory().toString() + "/Movies/" + mApp.getUrl()));

        player.prepare(mediaSource);
        player.seekTo((long)currentPosition);
        player.setPlayWhenReady(isPlay);

//        mHandler = new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//                if(msg.what == READ_FINISH) {
//                    currentPosition = Float.parseFloat(Objects.requireNonNull(videoTransmission.getContactList().get(0).get("seek")));
//
//                    if (Objects.requireNonNull(videoTransmission.getContactList().get(0).get("play")).equals("true")) isPlay = true;
//                    else if (Objects.requireNonNull(videoTransmission.getContactList().get(0).get("play")).equals("false")) isPlay = false;
//
//                    if(!isPlay) player.seekTo((long)currentPosition);
//
//                    player.setPlayWhenReady(isPlay);
//
//                    videoTransmission.read(READ_FINISH, mHandler);
//                }
//            }
//        };
    }

//    private class BackgroundThread extends Thread {
//        public void run() {
//            while(isThread) {
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//
//                mHandler.sendEmptyMessage(1);
//            }
//        }
//    }
}