package com.samwoo.sampleworker;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;

public class LoadingActivity extends AppCompatActivity {
    SmartWorkApplication mApp;

    ImageView loading;

    ValueAnimator vAnim = null;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_loading);

        mApp = (SmartWorkApplication)getApplication();

        mApp.setSerial(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

        loading = findViewById(R.id.loading);

        vAnim = makeValueAnimator(loading, 1000, 0.0f, 1.0f);
        vAnim.start();
    }

    public ValueAnimator makeValueAnimator(final View view, long duration, float fromAlpha, float toAlpha) {
        ValueAnimator valueAnimator = null;

        valueAnimator = ValueAnimator.ofFloat(fromAlpha, toAlpha);

        valueAnimator.setDuration(duration);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                view.setAlpha((float)animation.getAnimatedValue());
            }
        });

        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (view.getAlpha() >= 1.0f) {
                    vAnim = makeValueAnimator(view, 2000, 1.0f, 0.0f);
                    vAnim.start();
                }
                else {
                    mApp.setLoading(true);

                    Intent i;
                    i = new Intent(LoadingActivity.this, TaskService.class);
                    startService(i);
                    i = new Intent(LoadingActivity.this, ScanLoginActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) { }

            @Override
            public void onAnimationRepeat(Animator animation) { }
        });

        return valueAnimator;
    }
}
