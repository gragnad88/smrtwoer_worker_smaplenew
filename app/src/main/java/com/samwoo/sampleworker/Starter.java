package com.samwoo.sampleworker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;

public class Starter extends BroadcastReceiver {
    SmartWorkApplication mApp;

    DBUtils auth = new DBUtils();
    DBUtils device_state = new DBUtils();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (context.getApplicationContext() instanceof SmartWorkApplication) mApp = ((SmartWorkApplication)context.getApplicationContext());

        auth.setDB(null, Constant.AUTH, new String[]{ "userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" });
        //auth.read();
        device_state.setDB(null, Constant.DEVICE_STATE, new String[]{ "serial", "boot_time", "battery", "longitude", "latitude" });
        //device_state.read();

        Intent i = new Intent(context, LoadingActivity.class);
        assert action != null;
        if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } else if (action.equals("android.intent.action.ACTION_SHUTDOWN")) {
            String auth_id = mApp.getAuthID();
            String auth_user_id = mApp.getId();
            String auth_user_pw = mApp.getPassword();
            String auth_user_name = mApp.getWorkerName();
            String auth_manager = mApp.getManager();

            auth.update(auth_id, new String[]{ auth_user_id, auth_user_pw, auth_user_name, auth_manager, "", "", "" });
            device_state.update(mApp.getDeviceStateID(), new String[]{ mApp.getSerial(), "", "", "", "" });

            // php 에게 update 쿼리를 넘겨주기전에 종료되는 것을 방지
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(i);
        } else {
            context.startService(i);
        }
    }
}