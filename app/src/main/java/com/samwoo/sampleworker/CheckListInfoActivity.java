package com.samwoo.sampleworker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CheckListInfoActivity extends AppCompatActivity {
    SmartWorkApplication mApp;

    private static final int CAMERA_REQUEST_CODE = 1992;
    private static final int DOWNLOAD_FINISH = 7000;
    private static final int CHECKLIST_FINISH = 7001;

    DBUtils DB_checklist = new DBUtils();
    DBUtils DB_checklist_info = new DBUtils();

    Button backBtn;
    Button nextBtn;
    Button checkBox;
    TextView textView;
    ImageView imageView;
    WebView logo_img;
    TextView workerName, workerNumber;

    Handler mHandler = null;

    List<String> image = new ArrayList<>();
    List<String> check = new ArrayList<>();
    List<String> text = new ArrayList<>();
    List<String> id = new ArrayList<>();

    int listIndex = 0;
    int downloadFinish = 0;
    Dialog dialog;
    String fileName;

    String save_Path;
    String save_folder = "/Download/";
    DownloadThread dThread;
    //
    Runnable readWorkThread = new Runnable() {
        @Override
        public void run() {
            for (int i = 0; i < DB_checklist_info.getContactList().size(); i++) {
                if (Objects.requireNonNull(DB_checklist_info.getContactList().get(i).get("u_index")).equals(mApp.getChecklist())) {
                    image.add(DB_checklist_info.getContactList().get(i).get("u_image"));
                    check.add(DB_checklist_info.getContactList().get(i).get("u_check"));
                    text.add(DB_checklist_info.getContactList().get(i).get("u_text"));
                    id.add(DB_checklist_info.getContactList().get(i).get("id"));
                }
            }

            downloadFinish = image.size();

            for (int i = 0; i < image.size(); i++) {
                if (!image.get(i).equals("no_image")) fileDownload(image.get(i));
                else {
                    downloadFinish--;
                    if (downloadFinish == 0) {
                        mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
                        break;
                    }
                }
            }
        }
    };
    //
    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_check_list_info);

        mApp = (SmartWorkApplication)getApplication();

        textView = (TextView)findViewById(R.id.checkListText);
        backBtn = (Button)findViewById(R.id.backButton);
        nextBtn = (Button)findViewById(R.id.nextButton);
        checkBox = (Button)findViewById(R.id.checkbox);
        imageView = (ImageView)findViewById(R.id.checklistImage);
        logo_img = (WebView)findViewById(R.id.logo);
        workerName = (TextView)findViewById(R.id.workerName);
        workerNumber = (TextView)findViewById(R.id.workerNumber);

        int width = 153;
        String url = "http://54.180.178.2/uploads/" + mApp.getManager() + "_logo.png";

        String data = "<html><head><title>Example</title><meta name=\"viewport\"\"content=\"width=" + width + ", initial-scale=0.65 \" /></head>";
        data = data + "<body><center><img width=\"" + width + "\" src=\"" + url + "\" /></center></body></html>";

        logo_img.loadData(data, "text/html", null);
        workerName.setText("사원 이름: " + mApp.getWorkerName());
        workerNumber.setText("사원 번호: " + mApp.getAuthID());

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case DBUtils.DBTableType.CHECKLIST_INFO_READ:
                            Thread t = new Thread(readWorkThread);
                            t.start();
                        break;
                    case DOWNLOAD_FINISH:
                        if (image.get(listIndex).equals("no_image")) {
                            imageView.setImageBitmap(null);
                            imageView.setBackgroundResource(R.drawable.no_image);
                        } else {
                            File imgFile = new File(Environment.getExternalStorageDirectory().toString() + "/Download/" + image.get(listIndex));

                            if (imgFile.exists()) {
                                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                                imageView.setBackground(null);
                                imageView.setImageBitmap(myBitmap);
                            }
                        }

                        textView.setText(text.get(listIndex));

                        if (listIndex == check.size() - 1) {
                            nextBtn.setText("완료");
                            nextBtn.setBackgroundResource(R.drawable.btn_finish_checklist);
                        }

                        backBtn.setVisibility(View.INVISIBLE);

                        if (!check.get(listIndex).equals("0")) {
                            checkBox.setBackgroundResource(R.drawable.checkbox_true);
                        }
                        break;
                    case CHECKLIST_FINISH:
                        finish();
                        break;
                }
            }
        };

        DB_checklist.setDB(this, Constant.CHECKLIST, new String[]{ "name", "date", "worker","capture" });
        DB_checklist.read();
        DB_checklist_info.setDB(this, Constant.CHECKLIST_INFO, new String[]{ "u_index", "u_image", "u_text", "u_check" });
        DB_checklist_info.read(DBUtils.DBTableType.CHECKLIST_INFO_READ, mHandler);

        String ext = Environment.getExternalStorageState();
        if (ext.equals(Environment.MEDIA_MOUNTED)) save_Path = Environment.getExternalStorageDirectory().getAbsolutePath();

        backBtn.setOnClickListener(v -> {
            if (listIndex > 0){
                listIndex--;

                nextBtn.setText("다음");
                nextBtn.setBackgroundResource(R.drawable.btn_next_checklist);
            }

            if (listIndex == 0)
                backBtn.setVisibility(View.INVISIBLE);

            if (image.get(listIndex).equals("no_image")) {
                imageView.setImageBitmap(null);
                imageView.setBackgroundResource(R.drawable.no_image);
            } else {
                File imgFile = new File(Environment.getExternalStorageDirectory().toString() + "/Download/" + image.get(listIndex));

                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                    imageView.setBackground(null);
                    imageView.setImageBitmap(myBitmap);
                }
            }

            textView.setText(text.get(listIndex));

            if (!check.get(listIndex).equals("0")) {
                checkBox.setBackgroundResource(R.drawable.checkbox_true);
            } else {
                checkBox.setBackgroundResource(R.drawable.checkbox_false);
            }
        });

        nextBtn.setOnClickListener(v -> {
            if (listIndex < check.size() - 1){
                listIndex++;

                backBtn.setVisibility(View.VISIBLE);
            } else {
                for (int i = 0 ; i < check.size(); i++) {
                    DB_checklist_info.update(id.get(i), new String[]{ mApp.getChecklist(), image.get(i), text.get(i), check.get(i) });
                }

                int count = 0;

                for (int i = 0; i < check.size(); i++) {
                    if (!check.get(i).equals("0")) count++;
                }

                if (count == check.size()) {
                    @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    fileName = mApp.getWorkerName() + "_" + timestamp + ".png";

                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File file = new File(save_Path + "/Pictures", fileName);

                    Uri outputFileUri;
                    outputFileUri = Uri.fromFile(file);

                    i.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                    startActivityForResult(i, CAMERA_REQUEST_CODE);
                } else {
                    DB_checklist.update(mApp.getChecklist(), new String[]{ mApp.getEquipment(), "", "", "" }, mHandler, CHECKLIST_FINISH);

                    Toast.makeText(CheckListInfoActivity.this, "작업 내용을 저장합니다.", Toast.LENGTH_LONG).show();
                }
            }

            if (listIndex == check.size() - 1) {
                nextBtn.setText("완료");
                nextBtn.setBackgroundResource(R.drawable.btn_finish_checklist);
            }

            if (image.get(listIndex).equals("no_image")) {
                imageView.setImageBitmap(null);
                imageView.setBackgroundResource(R.drawable.no_image);
            } else {
                File imgFile = new File(Environment.getExternalStorageDirectory().toString() + "/Download/" + image.get(listIndex));

                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                    imageView.setBackground(null);
                    imageView.setImageBitmap(myBitmap);
                }
            }

            textView.setText(text.get(listIndex));

            if (!check.get(listIndex).equals("0")) {
                checkBox.setBackgroundResource(R.drawable.checkbox_true);
            } else {
                checkBox.setBackgroundResource(R.drawable.checkbox_false);
            }
        });

        checkBox.setOnClickListener(v -> {
            if (!check.get(listIndex).equals("0")) {
                checkBox.setBackgroundResource(R.drawable.checkbox_false);
                check.set(listIndex, "0");
            } else {
                checkBox.setBackgroundResource(R.drawable.checkbox_true);
                check.set(listIndex, "1");
            }
        });
    }

    private class DownloadThread extends Thread {
        String ServerUrl;
        String LocalPath;

        DownloadThread(String serverPath, String localPath) {
            ServerUrl = serverPath;
            LocalPath = localPath;
        }

        @Override
        public void run() {
            URL imgurl;
            int Read;
            try {
                imgurl = new URL(ServerUrl);
                HttpURLConnection conn = (HttpURLConnection) imgurl.openConnection();
                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];
                InputStream is = conn.getInputStream();
                File file = new File(LocalPath);
                FileOutputStream fos = new FileOutputStream(file);

                for (;;) {
                    Read = is.read(tmpByte);
                    if (Read <= 0) break;
                    fos.write(tmpByte, 0, Read);
                }

                downloadFinish--;
                if(downloadFinish == 0) mHandler.sendEmptyMessage(DOWNLOAD_FINISH);

                is.close();
                fos.close();
                conn.disconnect();
            } catch (MalformedURLException e) {
                Log.e("ERROR1", e.getMessage());
            } catch (IOException e) {
                Log.e("ERROR2", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void fileDownload(String file) {
        File dir = new File(save_Path + save_folder);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (!new File(save_Path + save_folder + file).exists()) {
            dThread = new DownloadThread(Constant.FILE_PATH + "/" + file,
                    save_Path + save_folder + file);
            dThread.start();
        } else {
            downloadFinish--;
            if(downloadFinish == 0) mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                dialog = ProgressDialog.show(CheckListInfoActivity.this, "", "Uploading File...", true);

                String rootSD = Environment.getExternalStorageDirectory().toString();

                new Thread(() -> uploadFile(rootSD + "/Pictures/" + fileName)).start();

                @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date currentTime = new Date();
                String now = formatter.format(currentTime);

                DB_checklist.update(mApp.getChecklist(), new String[]{ mApp.getEquipment(), now, mApp.getWorkerName(), fileName }, mHandler, CHECKLIST_FINISH);

                Toast.makeText(CheckListInfoActivity.this, "작업 내용을 저장합니다.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("LongLogTag")
    public void uploadFile(final String sourceFileUri) {
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "********";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1024 * 1024;

        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {
            dialog.dismiss();
            Log.e("uploadFile", "Source File not exist :" + "Pictures/" + sourceFileUri);
        } else {
            try {
                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Constant.FILE_PATH_PHP);

                // Open a HTTP  connection to  the URL
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod(Constant.POST_METHOD);
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", sourceFileUri);

                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"");
                dos.write(sourceFileUri.getBytes("utf-8"));
                dos.writeBytes("\"" + lineEnd);
                dos.writeBytes(lineEnd);
//                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + sourceFileUri + "\"" + lineEnd);
//                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);

                buffer = new byte[bufferSize];
                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                int serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

                if(serverResponseCode == 200) {
                    runOnUiThread(() -> Toast.makeText(CheckListInfoActivity.this, "File Upload Complete.", Toast.LENGTH_SHORT).show());
                }
                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                dialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(() -> Toast.makeText(CheckListInfoActivity.this, "MalformedURLException", Toast.LENGTH_SHORT).show());

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(() -> Toast.makeText(CheckListInfoActivity.this, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show());
                Log.e("Upload file to server Exception", "Exception : " + e.getMessage(), e);
            }
            dialog.dismiss();
        } // End else block
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
