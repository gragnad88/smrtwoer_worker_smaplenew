package com.samwoo.sampleworker;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class FileMenu extends AppCompatActivity {
    private static final int IMAGE = 1;
    private static final int VIDEO = 2;

    private List<String> myList;
    private ImageView imageView;
    private VideoView videoView;
    private ListView listView;
    Button imageBtn;
    Button videoBtn;
    Button textBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_file_menu);

        imageView = (ImageView)findViewById(R.id.imageView);
        videoView = (VideoView)findViewById(R.id.videoView);
        listView = (ListView)findViewById(R.id.listView);
        imageBtn = (Button)findViewById(R.id.imageBtn);
        videoBtn = (Button)findViewById(R.id.videoBtn);
        textBtn  = (Button)findViewById(R.id.textBtn);

        MediaController mc = new MediaController(this);
        videoView.setMediaController(mc);
        myList = new ArrayList<>();
    }

    public void mOnClick(View v) {
        imageView.setVisibility(INVISIBLE);
        videoView.setVisibility(INVISIBLE);
        listView.setVisibility(INVISIBLE);

        switch (v.getId()) {
            // 이미지
            case R.id.imageBtn:
//                imageBtn.setBackgroundResource(R.drawable.btn_image_pressed);
//                videoBtn.setBackgroundResource(R.drawable.btn_video);
//                textBtn.setBackgroundResource(R.drawable.btn_text);
                startActivityForResult(Intent.createChooser(new Intent(Intent.ACTION_GET_CONTENT).setType("image/*"), "Select Image"), IMAGE);
                break;
            // 동영상
            case R.id.videoBtn:
//                imageBtn.setBackgroundResource(R.drawable.btn_image);
//                videoBtn.setBackgroundResource(R.drawable.btn_video_pressed);
//                textBtn.setBackgroundResource(R.drawable.btn_text);
                startActivityForResult(Intent.createChooser(new Intent(Intent.ACTION_GET_CONTENT).setType("video/*"), "Select Video"), VIDEO);
                break;
            // 문서
            case R.id.textBtn:
//                imageBtn.setBackgroundResource(R.drawable.btn_image);
//                videoBtn.setBackgroundResource(R.drawable.btn_video);
//                textBtn.setBackgroundResource(R.drawable.btn_text_pressed);

                listView.setVisibility(VISIBLE);

                String fileType1 = ".pdf";
                String fileType2 = ".txt";

                String rootSD = Environment.getExternalStorageDirectory().toString();
                File file = new File(rootSD + "/download");
                final File[] list = file.listFiles();

                myList.clear();

                for (File file1 : list) {
                    if (file1.getName().endsWith(fileType1) || file1.getName().endsWith(fileType2))
                        myList.add(file1.getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, myList);

                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                listView.setOnItemClickListener((parent, view, position, id) -> {
                    String strText = (String) parent.getItemAtPosition(position) ;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    File openFile = null;

                    for (File aList : list) {
                        if (strText.equals(aList.getName()))
                            openFile = aList.getAbsoluteFile();
                    }

                    assert openFile != null;
                    if(openFile.getName().endsWith(".pdf")) i.setDataAndType(Uri.fromFile(openFile), "application/*");
                    else i.setDataAndType(Uri.fromFile(openFile), "text/*");

                    startActivity(i);
                });

                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE) {
                imageView.setVisibility(VISIBLE);

                try {
                    InputStream is = getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));

                    Bitmap img = BitmapFactory.decodeStream(is);
                    assert is != null;
                    is.close();

                    imageView.setImageBitmap(img);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == VIDEO) {
                videoView.setVisibility(VISIBLE);

                videoView.setVideoURI(data.getData());
                videoView.requestFocus();
                videoView.start();
            }
        }
    }
}