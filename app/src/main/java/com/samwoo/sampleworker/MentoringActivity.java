package com.samwoo.sampleworker;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;

import android.content.pm.ActivityInfo;

import android.os.Build;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.net.URISyntaxException;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.samwoo.sampleworker.DB.Constant;

import org.json.JSONException;
import org.json.JSONObject;

public class MentoringActivity extends AppCompatActivity {
    SmartWorkApplication mApp;

    private static final String SERVER_URL = Constant.IP + ":10001";

    private static final String SEND_MESSAGE = "client-message";
    private static final String RECEIVE_CONNECT_ERROR_MESSAGE = "worker_disconnect_complete";
    private Socket socket;

    Handler mHandler = null;

    JSONObject receivedData;
    WebView webView;

    @SuppressLint({"SetJavaScriptEnabled", "HandlerLeak"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_mentoring);

        mApp = (SmartWorkApplication)getApplication();

        receivedData = new JSONObject();

         mHandler = new Handler() {
             @Override
             public void handleMessage(Message msg) {
                 if (msg.what == 9) {
                     disconnect();
                     webView.destroy();
                     finish();

                     Toast.makeText(getApplicationContext(), "관리자가 원격멘토를 종료하였습니다.", Toast.LENGTH_LONG).show();
                 }
             }
         };

        webView = (WebView) findViewById(R.id.webView);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.setWebChromeClient(new WebChromeClient() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                MentoringActivity.this.runOnUiThread(() -> request.grant(request.getResources()));
            }
        });

        webView.loadUrl("https://appr.tc/r/" + mApp.getRoomNumber());

//        webView.loadUrl("https://vlaos-smartwork.com/demos/video-conference/video-conference.html?open=false&sessionid=" +
//                mApp.getRoomNumber() + "&publicRoomIdentifier=video-conference-dashboard&userFullName=" + mApp.getWorkerName() + "&manager=false");
    }

    @Override
    protected void onResume() {
        super.onResume();

        connect();
    }

    @Override
    protected void onPause() {
        super.onPause();

        disconnect();
    }

    private boolean connect() {
        try {
            socket = IO.socket(SERVER_URL);
            socket.on(Socket.EVENT_CONNECT, onConnect);
            socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            socket.on(RECEIVE_CONNECT_ERROR_MESSAGE, onReceiveConnectErrorMessage);
            socket.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    private void disconnect() {
        socket.disconnect();
        socket.off(Socket.EVENT_CONNECT, onConnect);
        socket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        socket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        socket.off(RECEIVE_CONNECT_ERROR_MESSAGE, onReceiveConnectErrorMessage);
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendMessage();
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // your code...
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // your code...
        }
    };

    private Emitter.Listener onReceiveConnectErrorMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // 전달받은 데이터는 아래와 같이 추출할 수 있습니다.
            try {
                receivedData.put(RECEIVE_CONNECT_ERROR_MESSAGE, args[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Message msg = Message.obtain(null, 9);
            msg.obj = receivedData;
            mHandler.sendMessage(msg);
//            receivedData = (JSONObject) args[0];
            // your code...
        }
    };

    private void sendMessage() {
        JSONObject data = new JSONObject();
        try {
            data.put("id", mApp.getId());
            data.put("manager", "false");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit(SEND_MESSAGE, data);
    }

    @Override
    public void onBackPressed() {
        // mWorkThread.interrupt();
        super.onBackPressed();
        disconnect();
        webView.destroy();
        finish();
    }
}