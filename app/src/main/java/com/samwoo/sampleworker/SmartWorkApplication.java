package com.samwoo.sampleworker;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.samwoo.sampleworker.AbstractActivity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class SmartWorkApplication extends Application {
    private String serial;
    private String deviceStateID;
    private String id;
    private String password;
    private String url;
    private String workerName;
    private String checklist;
    private String equipment;
    private String authID;
    private String manager;
    private String roomNumber;
    private boolean controlVideo = true;
    private boolean loading = false;

    public SmartWorkApplication() { }

    public interface ActivityName_KEY {
        final int MAIN_ACTIVITY_TAG = 3001;
        final int CHECKLIST_ITEM_ACTIVITY_TAG = 3002;
        final int CHECKLIST_INFO_ACTICITY_TAG = 3003;
        final int MENTORING_ACTIVITY_TAG = 3004;
        final int FILE_ACTIVITY_TAG = 3005;
        final int VIDEO_PLAYER_ACTIVITY_TAG = 3006;
    }

    public enum AppStatus {
        BACKGROUND,
        RETURNED_TO_FOREGROUND,
        FOREGROUND
    }

    HashMap<Integer,AppStatus> activityHashMap = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();

        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks);
    }

    public void AddActivityForStart(Integer _ActivityKeyData) { activityHashMap.put(_ActivityKeyData,AppStatus.FOREGROUND); }

    public void setChangeActivityStatus(Integer _ActivityKeyData,AppStatus _ActivityStatus) {
       Iterator<Map.Entry<Integer,AppStatus>> itr = activityHashMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Integer,AppStatus> obj =  itr.next();
            if(obj.getKey().equals(_ActivityKeyData)) {
                obj.setValue(_ActivityStatus);
                Log.i("Activity Hash Map : ",obj.getKey()+" : " +obj.getValue());
            }
        }
    }

    public AppStatus getActivitystatus(Integer _ActivityKeyData) {
        AppStatus _status = AppStatus.BACKGROUND;
        Iterator<Map.Entry<Integer,AppStatus>> itr = activityHashMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Integer,AppStatus> obj =  itr.next();
            if(obj.getKey().equals(_ActivityKeyData)) {
                _status = obj.getValue();
            }
        }
        return  _status;
    }

    ActivityLifecycleCallbacks mActivityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if(activity instanceof AbstractActivity) {
                AbstractActivity startActivity= (AbstractActivity)activity;
                Log.i("Application Create : ",startActivity.mActivityKeyName.toString());
                AddActivityForStart(startActivity.mActivityKeyName);
            }
        }

        @Override
        public void onActivityStarted(Activity activity) { }

        @Override
        public void onActivityResumed(Activity activity) {
            if(activity instanceof AbstractActivity) {
                AbstractActivity startActivity = (AbstractActivity) activity;
                setChangeActivityStatus(startActivity.mActivityKeyName, AppStatus.RETURNED_TO_FOREGROUND);
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            if(activity instanceof AbstractActivity) {
                AbstractActivity startActivity = (AbstractActivity) activity;
                setChangeActivityStatus(startActivity.mActivityKeyName, AppStatus.BACKGROUND);
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {
//            if(activity instanceof AbstractActivity) {
//                AbstractActivity startActivity = (AbstractActivity) activity;
//                setChangeActivityStatus(startActivity.mActivityKeyName, AppStatus.BACKGROUND);
//            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) { }

        @Override
        public void onActivityDestroyed(Activity activity) { }
    };

    public String getSerial() { return serial; }

    public void setSerial(String serial) {  this.serial = serial; }

    public String getDeviceStateID() { return deviceStateID; }

    public void setDeviceStateID(String deviceStateID) { this.deviceStateID = deviceStateID; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public String getWorkerName() { return workerName; }

    public void setWorkerName(String workerName) { this.workerName = workerName; }

    public String getChecklist() { return checklist; }

    public void setChecklist(String checklist) { this.checklist = checklist; }

    public String getEquipment() { return equipment; }

    public void setEquipment(String equipment) { this.equipment = equipment; }

    public String getAuthID() { return authID; }

    public void setAuthID(String authID) { this.authID = authID; }

    public String getManager() { return manager; }

    public void setManager(String manager) { this.manager = manager; }

    public String getRoomNumber() { return roomNumber; }

    public void setRoomNumber(String roomNumber) { this.roomNumber = roomNumber; }

    public boolean isControlVideo() { return controlVideo; }

    public void setControlVideo(boolean controlVideo) { this.controlVideo = controlVideo; }

    public boolean isLoading() { return loading; }

    public void setLoading(boolean loading) { this.loading = loading; }
}