package com.samwoo.sampleworker;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;

import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@SuppressLint("Registered")
public class ScanLoginActivity extends AppCompatActivity {
    SmartWorkApplication mApp;

    private String idByANDROID_ID;
    String gMData = "[No Barcode]";

    Handler mHandler = null;

    DBUtils auth;

    BlockingQueue<DBUtils.QueryResultData> DBinData;
    @SuppressLint({"HardwareIds", "HandlerLeak"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApp = (SmartWorkApplication)getApplication();
        //
        idByANDROID_ID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        //
        DBinData = new ArrayBlockingQueue<>(10);

        auth = new DBUtils();
    
        auth.setDB(this,Constant.AUTH, new String[]{ "userID", "userPW", "userNAME", "manager", "serial", "state", "state_time" });
        auth.read();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (msg.what == 1) {
                    if (!isMyServiceRunning()) {
                        startService(new Intent(ScanLoginActivity.this, TaskService.class));
                    }

                    Intent i = new Intent(ScanLoginActivity.this, MainActivity.class);
                    startActivity(i);
                }
            }
        };
    }

    protected void onResume(){
        super.onResume();

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(LoginActivity.class);
        integrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result.getFormatName().equals(IntentIntegrator.QR_CODE)) {
            if (data != null) {
                gMData = result.getContents();

                if (gMData.endsWith("..auth")) loginQR(gMData);
                else Toast.makeText(this, "작업자 정보가 틀립니다.\n사원증 확인 후 다시 인식 해주세요.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void loginQR(String str) {
        String id;
        String pw;

        id = str.substring(0, str.indexOf("///"));
        pw = str.substring(str.indexOf("///") + 3, str.indexOf("..auth"));


        for (int i = 0; i < auth.contactList.size(); i++) {
            if (Objects.requireNonNull(auth.contactList.get(i).get("userID")).equals(id) && Objects.requireNonNull(auth.contactList.get(i).get("userPW")).equals(pw)) {
                mApp.setId(auth.contactList.get(i).get("userID"));
                mApp.setPassword(auth.contactList.get(i).get("userPW"));
                mApp.setWorkerName(auth.contactList.get(i).get("userNAME"));
                mApp.setManager(auth.contactList.get(i).get("manager"));
                mApp.setAuthID(auth.contactList.get(i).get("id"));

                auth.update(auth.contactList.get(i).get("id"),
                        new String[]{ auth.contactList.get(i).get("userID"),
                        auth.contactList.get(i).get("userPW"),
                        auth.contactList.get(i).get("userNAME"),
                        auth.contactList.get(i).get("manager"),
                        idByANDROID_ID, "", "" });

                Toast.makeText(this, mApp.getWorkerName()+ "님 환영합니다.", Toast.LENGTH_LONG).show();


                mHandler.sendEmptyMessage(1);

                finish();

                break;
            }

            if (i + 1 == auth.getContactList().size()) Toast.makeText(this, "작업자 정보가 틀립니다.\n사원증 확인 후 다시 인식 해주세요.", Toast.LENGTH_LONG).show();
        }

        gMData = "[No Barcode]";
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (TaskService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
