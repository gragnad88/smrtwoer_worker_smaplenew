package com.samwoo.sampleworker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.samwoo.sampleworker.DB.Constant;
import com.samwoo.sampleworker.DB.DBUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class TaskService extends Service {
    SmartWorkApplication mApp;

    private String serial;
    private String boot_time;
    String battery;

    Handler mHandler;

    DBUtils auth = new DBUtils();
    DBUtils device_state = new DBUtils();

    private LocationListener mLocationListener;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super .onStartCommand(intent, flags, startId);
    }
    */

    LocationManager mLocationManager;

    private Location getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return TODO;
            int a = 0;
            a= 3;
        }


        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {

            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    @SuppressLint({"HardwareIds", "HandlerLeak"})
    @Override
    public void onCreate() {
        super.onCreate();

        mApp = (SmartWorkApplication)getApplication();

        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

        serial = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        long time = System.currentTimeMillis() - SystemClock.elapsedRealtime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String month = Integer.toString(calendar.get(Calendar.MONTH) + 1);
        String date = Integer.toString(calendar.get(Calendar.DATE));
        String hour = Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
        String minute = Integer.toString(calendar.get(Calendar.MINUTE));
        String second = Integer.toString(calendar.get(Calendar.SECOND));
        boot_time = month + "월" + date + "일 " + hour + "시" + minute + "분" + second + "초";

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == DBUtils.DBTableType.DEVICE_STATE_READ) {
                    Log.i("Start TaskService" , "Location Service");
                    startForegroundService();
                }
            }
        };

        mLocationListener = new LocationListener() {

            public void onLocationChanged(Location location) {

                Log.d("test", "onLocationChanged, location:" + location);
                double longitude = location.getLongitude(); //경도
                double latitude = location.getLatitude();   //위도

                battery = Integer.toString(getBatteryPercentage(TaskService.this));

                if(device_state.getContactList().size() == 0)
                {
                    device_state.insert(new String[]{serial, boot_time, battery, Double.toString(longitude), Double.toString(latitude)});
                }

                else if(device_state.getContactList().size() != 0){
                    for (int i = 0; i < device_state.getContactList().size(); i++) {
                        if (Objects.requireNonNull(device_state.getContactList().get(i).get("serial")).equals(serial)) {
                            mApp.setDeviceStateID(device_state.getContactList().get(i).get("id"));
                            device_state.update(device_state.getContactList().get(i).get("id"), new String[]{serial, boot_time, battery, Double.toString(longitude), Double.toString(latitude)});
                            break;
                        }
                    }
                }
            }

            public void onProviderDisabled(String provider) {
                // Disabled 시
                Log.d("test", "onProviderDisabled, provider:" + provider);
            }

            public void onProviderEnabled(String provider) {
                // Enabled 시
                Log.d("test", "onProviderEnabled, provider:" + provider);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                // 변경 시
                Log.d("test", "onStatusChanged, provider:" + provider + ", status:" + status + ", Bundle:" + extras);
            }
        };



        auth.setDB(null, Constant.AUTH, new String[]{"userID", "userPW", "userNAME", "manager", "serial", "state", "state_time"});
        auth.read();
        device_state.setDB(null, Constant.DEVICE_STATE, new String[]{"serial", "boot_time", "battery", "longitude", "latitude"},true);
        device_state.read(DBUtils.DBTableType.DEVICE_STATE_READ, mHandler);



        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                super.run();
            }
        });
    }

    @Override
    public void onDestroy() {
        String auth_id = mApp.getAuthID();
        String auth_user_id = mApp.getId();
        String auth_user_pw = mApp.getPassword();
        String auth_user_name = mApp.getWorkerName();
        String auth_manager = mApp.getManager();

        auth.update(auth_id, new String[]{auth_user_id, auth_user_pw, auth_user_name, auth_manager, "", "", ""});
        //
        for (int i = 0; i < device_state.getContactList().size(); i++) {
            if (Objects.requireNonNull(device_state.getContactList().get(i).get("serial")).equals(serial))
                device_state.update(device_state.getContactList().get(i).get("id"), new String[]{serial, boot_time, "", "", ""});

        }

        // php 에게 update 쿼리를 넘겨주기전에 종료되는 것을 방지
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }


    private void startForegroundService() {
        // LocationManager 객체를 얻어온다
//        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        lm.requestSingleUpdate(LocationManager.GPS_PROVIDER, mLocationListener, getMainLooper());
//        lm.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, mLocationListener, getMainLooper());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, mLocationListener, getMainLooper());
        mLocationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, mLocationListener, getMainLooper());

        try {
            // GPS 제공자의 정보가 바뀌면 콜백하도록 리스너 등록하기~!!
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, // 등록할 위치제공자
                    2500, // 통지사이의 최소 시간간격 (miliSecond)
                    0, // 통지사이의 최소 변경거리 (m)
                    mLocationListener);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, // 등록할 위치제공자
                    2500, // 통지사이의 최소 시간간격 (miliSecond)
                    0, // 통지사이의 최소 변경거리 (m)
                    mLocationListener);
        } catch(SecurityException ignored) { }

    }


    public static int getBatteryPercentage(Context context) {
        Intent batteryStatus = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        assert batteryStatus != null;
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;
        return (int)(batteryPct * 100);
    }

    public void oneTimeQuery()
    {

    }


//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        super.onTaskRemoved(rootIntent);
//
//        String auth_id = mApp.getAuthID();
//        String auth_user_id = mApp.getId();
//        String auth_user_pw = mApp.getPassword();
//        String auth_user_name = mApp.getWorkerName();
//        String auth_manager = mApp.getManager();
//
//        auth.update(auth_id, auth_user_id, auth_user_pw, auth_user_name, auth_manager, "");
//        for (int i = 0; i < device_state.getContactList().size(); i++) {
//            if (Objects.requireNonNull(device_state.getContactList().get(i).get("serial")).equals(serial)) device_state.update(device_state.getContactList().get(i).get("id"), serial, boot_time, "", "", "");
//        }
//
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        this.stopSelf();
//    }
}
